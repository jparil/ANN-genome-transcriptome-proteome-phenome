#Rscript source function: perform genome wide association analysis in least squares approach given individual genotype/transcript and phenotype data

# INPUTS:
#1	Y --> 	vector of phenotypic values (nx1)
#2	X --> 	matrix of genotypic/transcript data (nxm)

# OUTPUT:
#1	LOD --> list of -log p-values (1xm)

GWAS_TWAS <- function(Y, X) {
	p_values = c()
	progressBar = txtProgressBar(min=0, max=l, initial=0, width=50, style=3)
	for (i in 1:l) {
		p_values = c(p_values, summary(lm(Y[,1] ~ X[,i]))$coefficients[2,4])
		setTxtProgressBar(progressBar, i)
	}
	close(progressBar)

	LOD = -log(p_values, base=10)
	return(LOD)
}