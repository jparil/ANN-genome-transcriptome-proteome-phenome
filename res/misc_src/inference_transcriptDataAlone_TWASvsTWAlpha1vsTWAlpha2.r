#!/usr/bin/env Rscript
# production code to simulate: (1) individual geno & pheno (TWAS), (2) pooled geno & individual pheno (GWAplha), and (3) pooled geno & pheno (TWAlpha)
#model1: Y1 = XKABj + e; model2: Y2 = XC1 + XKABj + e;
args = commandArgs(trailingOnly=TRUE)
model = as.integer(args[1])	#1=fully multiplicative; 2=additive-multiplicative
n = as.integer(args[2])		#number of individuals
l = as.integer(args[3]) 	#number of loci
m = as.integer(args[4]) 	#number of transcripts = number of loci
q = as.integer(args[5])		#number of causal loci or QTL (can sampled from some distribution)
t = as.integer(args[6]) 	#number of causal transcripts (can sampled from some distribution)
k = as.numeric(args[7])		#Verror / Vmodel
trait = as.integer(args[8])		#trait state (for TWAlpha_2 only): 0=quantitative mean; 1=binary survival rate (use 1 for my herbicide resistance per tray pooled phenotyping methodology)
fileNamesSuffix = args[9]	#output filenames suffix
iterations = as.integer(args[10]) # number of simulation iterations

#############################
######## DEFINE FUNCTIONS
#############################

#########
###   ###
### 1 ### Simulate genomes, transcriptomes and phenotypes:
###   ###
#########
simulate_genomes_transcriptomes_phenotypes <- function(model, n, l, m, q, t, k) {
	additiveGeneEffectsOnly = FALSE
	additiveTranscriptEffectsOnly = FALSE

	#######
	##	 ##
	## X ##
	##	 ##
	#######
	#genotype matrix (n x l)
	#X = matrix(rbinom(n=(n*l), size=2, prob=0.5), nrow=n, byrow=T) #without LD
	freqREF = rbeta(n=l, shape1=2, shape2=2)
	freqALT = 1 - freqREF
	link = rbinom(n=l, size=1, prob=freqREF)
	nCHROM = 7
	X1=matrix(NA, nrow=n, ncol=l)
	X2=matrix(NA, nrow=n, ncol=l)
	progressBar = txtProgressBar(min = 0, max = n, initial = 0, style=3, width=50)
	for (ind in 1:n) {
		x1=c()
		x2=c()
		for (i in 1:l) {
			if (i%%(round(n/nCHROM))==1) {
				a1 = sample(c(0, 1), size=1, prob=c(freqREF[i], freqALT[i]))	#No LD between scaffolds
				a2 = sample(c(0, 1), size=1, prob=c(freqREF[i], freqALT[i]))
			} else {
				if (link[i-1]==link[i]) {
					if ((link[i-1]==0 & a1==0) | (link[i-1]==1 & a1==0)) {
						Pab = min(freqREF[i-1], freqREF[i]) / max(freqREF[i-1], freqREF[i])
						a1 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
						a2 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
					} else {
						Pab = min(freqALT[i-1], freqALT[i]) / max(freqALT[i-1], freqALT[i])
						a1 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
						a2 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab)) }
				} else {
					if ((link[i-1]==0 & a1==0) | (link[i-1]==1 & a1==0)) {
						Pab = min(freqREF[i-1], freqALT[i]) / max(freqREF[i-1], freqALT[i])
						a1 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
						a2 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
					} else {
						Pab = min(freqALT[i-1], freqREF[i]) / max(freqALT[i-1], freqREF[i])
						a1 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
						a2 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab)) } 
				}
			}
			x1 = c(x1, a1)
			x2 = c(x2, a2)
		}
		setTxtProgressBar(progressBar, ind)
		X1[ind,] = x1
		X2[ind,] = x2
	}
	close(progressBar)
	X = X1 + X2

	# #plotting ~LD (correlation)
	# distance=c()
	# LD = c()
	# progressBar = txtProgressBar(min = 0, max = ( ((l^2)-l) / 2 ), initial = 0, style=3, width=50); counter = 0
	# for (i in 1:(l-1)) {
	# 	for (j in (i+1):l) {
	# 		distance = c(distance, abs(i-j))
	# 		LD = c(LD, cor(X[,i], X[,j]))
	# 		setTxtProgressBar(progressBar, counter); counter = counter + 1
	# 	}
	# }
	# jpeg("Pairwise loci correlation - LD measure.jpg", quality=100, width=700, height=500)
	# plot(distance, abs(LD))
	# dev.off()

	# #building G-matrix
	# M_matrix = X
	# ref.Allele.freq = colSums(X)/(2*nrow(X))
	# F_matrix = matrix(rep(ref.Allele.freq, each=n), ncol=l, byrow=F)
	# P_matrix = 2*(F_matrix - 0.50)
	# Z_matrix = M_matrix - P_matrix
	# G_matrix = (Z_matrix %*% t(Z_matrix)) / 2*sum(ref.Allele.freq*(1-ref.Allele.freq))


	#######
	##	 ##
	## K ##
	##	 ##
	#######
	#loci and loci interaction effects (l x l)
		#QTL additive effects sampling parameters:
			# #beta distribution:
			# 	qa1=2
			# 	qa2=2
			#normal distribution:
				qa1=0 #mean additive qlt effect
				qa2=4 #additive qtl variance (maybe add another parameter as the ratio between additive and non-additive variance?!)
		#QTL interaction effects sampling parameters:
			# #beta distribution:
			# 	qi1=1
			# 	qi2=2
			#normal distribution:
				qi1=0 #mean non-additive qtl effect
				qi2=1 #non-additive qtl vatiance
		qtl.loc = sample(1:l, size=q, replace=F)
		#qtl.additive = matrix(rbeta(n=q, shape1=qa1, shape2=qa2), nrow=q, ncol=1)
		#qtl.additive = qtl.additive/sum(qtl.additive)
		qtl.additive = matrix(rnorm(n=q, mean=qa1, sd=sqrt(qa2)), nrow=q, ncol=1)
		k_vect = matrix(0, nrow=l, ncol=1)
		k_vect[qtl.loc,1] = qtl.additive
	K = diag(k_vect[,1]) #----> no interactions - completely additive gene effects! - off-diagonals are all zeros!
		if (additiveGeneEffectsOnly == FALSE) {
				#qtl.interactions = matrix(rbeta(n=q*q, shape1=qi1, shape2=qi2), nrow=q, ncol=q, byrow=TRUE)
				#qtl.interactions = qtl.interactions/sum(qtl.interactions)
				qtl.interactions = matrix(rnorm(n=q*q, mean=qi1, sd=sqrt(qi2)), nrow=q, ncol=q, byrow=TRUE)
				qtl.AxI = qtl.interactions
				diag(qtl.AxI) = qtl.additive
				k_matr = matrix(0, nrow=l, ncol=l)
				k_matr[qtl.loc, qtl.loc] = qtl.AxI
			K = k_matr # ---> additive (diagonals) and interaction (off-diagonals) effects
		}
	# #assesing qtl effects distributions:
	# plot(density(K[K!=0])) #whole matrix
	# plot(density(diag(K)[diag(K)!=0])) #addtive effects only
	# d = row(K) - col(K)
	# plot(density(K[d!=0][K[d!=0]!=0])) #all non-additive effects

	#######
	##	 ##
	## A ##
	##	 ##
	#######
	#genotype to transcript connection (l x m)
		tra.loc = sample(1:m, size=t, replace=F)
		a = matrix(0, nrow=q, ncol=t, byrow=T)
			for (i in seq(1, t)){a[,i]=sample(c(1, rep(0, each=q-1)), size=q, replace=F)}
		A = matrix(0, nrow=l, ncol=m)
	A[qtl.loc, tra.loc] = a

	#######
	##	 ##
	## B ##
	##	 ##
	#######
	#transcript (and transcript genotype?!) interactions effects (m x m)
		#transcript additive effects sampling parameters:
			# #beta distribution:
			# 	ta1=2
			# 	ta2=2
			#normal distribution:
				ta1=0 #mean additive transcript effect
				ta2=4 #additive transcript variance
		#transcript interaction effects sampling parameters:
			# #beta distribution:
			# 	ti1=0.25
			# 	ti2=1
			#normal distribution:
				ti1=0 #mean additive transcript effect
				ti2=1 #additive transcript variance
		# tra.additive = matrix(rbeta(n=t, shape1=ta1, shape2=ta2), nrow=t, ncol=1)
		# tra.additive = tra.additive/sum(tra.additive)
		tra.additive = matrix(rnorm(n=t, mean=ta1, sd=sqrt(ta2)), nrow=t, ncol=1)
		b_vect = matrix(0, nrow=m, ncol=1)
		b_vect[tra.loc,1] = tra.additive
	B = diag(b_vect[,1]) #----> no interactions - completely additive transcript effects! - off-diagonals are all zeros!
		if (additiveTranscriptEffectsOnly == FALSE) {
				# tra.interactions = matrix(rbeta(n=q*q, shape1=ti1, shape2=ti2), nrow=q, ncol=q, byrow=TRUE)
				# tra.interactions = tra.interactions/sum(tra.interactions)
				tra.interactions = matrix(rnorm(n=q*q, mean=ti1, sd=sqrt(ti2)), nrow=q, ncol=q, byrow=TRUE)
				tra.BxI = tra.interactions
				diag(tra.BxI) = tra.additive
				b_matr = matrix(0, nrow=m, ncol=m)
				b_matr[tra.loc, tra.loc] = tra.BxI
			B = b_matr # ---> additive (diagonals) and interaction (off-diagonals) effects
				#Assesing tra effects:
					#tra additive effects distribution
						#plot(density(tra.additive))
					#tra interaction effects distribution
						#plot(density(tra.interactions))
		}
	# #assesing transcript effects distributions:
	# plot(density(B[B!=0])) #whole matrix
	# plot(density(diag(B)[diag(B)!=0])) #addtive effects only
	# d = row(B) - col(B)
	# plot(density(B[d!=0][B[d!=0]!=0])) #all non-additive effects

	#######
	##	 ##
	## J ##
	##	 ##
	#######
	#vector of ones for summing up all effects per individual
	j = matrix(1, nrow=m, ncol=1)

	#######
	##	 ##
	## e ##
	##	 ##
	#######
	#residual effects ~ N(0, sd=??? --> such that h2=50%)
	if (model == 0) {
		Vmodel = var(X %*% K %*% j)
		Verror = k*Vmodel
		e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
	} else if (model == 1) {
		Vmodel = var(X %*% K %*% A %*% B %*% j)
		Verror = k*Vmodel
		e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
	} else if (model == 2) {
		Vmodel = var((X %*% K %*% j) + (X %*% K %*% A %*% B %*% j))
		Verror = k*Vmodel
		e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
	}

	#######
	##	 ##
	## Y ##
	##	 ##
	#######
	#phenotypes
	if (model == 0) {
		#snp model
		Y = (X %*% K %*% j) + e
	} else if (model == 1) {
		#fully multiplicative
		Y = (X %*% K %*% A %*% B %*% j) + e
	} else if (model ==2){
		#additive-multiplicative?
		Y = (X %*% K %*% j) + (X %*% K %*% A %*% B %*% j) + e
	}
	# jpeg("Density Plot of Simulated Phenotypes.jpeg")
	# plot(density(Y))
	# dev.off()

	#######
	##	 ##
	## T ##
	##	 ##
	#######
	#transcript abundance data - all transcripts
	t_causal = X %*% K %*% A #scaled abundance matrix of causal transcripts
	T = matrix(rbeta(n=n*m, shape1=0.5, shape2=2), nrow=n, ncol=m, byrow=T)
	T[,tra.loc] = t_causal[,tra.loc]

	return(list("X"=X, "T"=T, "Y"=Y, "qtl.loc"=qtl.loc, "qtl.additive"=qtl.additive))
}

#########
###   ###
### 2 ### Compute ROC and AUC
###   ###
#########
simple_roc.bobHorton <- function(labels, scores){labels <- labels[order(scores, decreasing=TRUE)]; data.frame(TPR=cumsum(labels)/sum(labels), FPR=cumsum(!labels)/sum(!labels), labels)}
simple_auc.erik <- function(TPR, FPR){mean(sample(TPR,l,replace=TRUE) > sample(FPR,l,replace=TRUE))}


#########
###   ###
### 3 ### Synthetic TWAlpha in R
###   ###
#########
compute_Alpha <- function(FREQ_MATRIX, SD, MIN, MAX, PERC, Q) {
	#determine the size of each pool based on their percentile values
	BINS = c(PERC, 1) - c(0, PERC)
	#find the major allele - that is the allele with the highes frequency across pools
	allele_means = colMeans(FREQ_MATRIX)
	allele_max_index = which.max(colSums(FREQ_MATRIX)); if(allele_max_index==1){allele="A"} else if(allele_max_index==2){allele="T"} else if(allele_max_index==3){allele="C"} else if(allele_max_index==4){allele="G"} else if(allele_max_index==5){allele="N"} else {allele="DEL"} #determine major allele identity
	FREQ_A = FREQ_MATRIX[,allele_max_index]	#potential problem here when more than 1 column or allele are equal to the maximum value!!!
	#transform the allele frequency of the major allele such that the sum of allele frequencies across pools is equal to 1
	BIN_A = FREQ_A*BINS/sum(FREQ_A*BINS); BIN_A = BIN_A/sum(BIN_A) #is dividing them again with the sum necessary???
	#transform the aggregate of the alternative alleles in the same way as in the major allele
	BIN_B = (1 - FREQ_A)*BINS/(1-sum(FREQ_A*BINS));	BIN_B = BIN_B/sum(BIN_B)
	#convert back the transformed allele frequencies into percentiles
	PERC_A = c(); PERC_B = c()
	for(i in 1:length(BIN_A)){
		PERC_A = c(PERC_A, sum(BIN_A[1:i]))
		PERC_B = c(PERC_B, sum(BIN_B[1:i]))
	}
	#define the likelihood function as the cdf(yi, b1,b2) - cdf(yi-1, b1,b2) [--> for the major allele] PLUS cdf(yi, b3,b4) - cdf(yi-1, b3,b4) [--> for the altenative alleles aggregate]
	optimize_me <- function(PERC_A, PERC_B, par) {
		PERC_A0 = c(0, PERC_A[1:(length(PERC_A)-1)])
		PERC_B0 = c(0, PERC_B[1:(length(PERC_B)-1)])
		return(-sum(log(pbeta(PERC_A, shape1=par[1], shape2=par[2])-pbeta(PERC_A0, shape1=par[1], shape2=par[2]))) - sum(log(pbeta(PERC_B, shape1=par[3], shape2=par[4])-pbeta(PERC_B0, shape1=par[3], shape2=par[4]))) )
	}
	#maximize the likelihood or in the case minimize the -log likelihood
	OPTIM_PAR = tryCatch(optim(par=c(1,1,1,1), fn=optimize_me, PERC_A=PERC_A, PERC_B=PERC_B)$par, error=function(e){return(c(1,1,1,1))})
	#compute for the mean of the beta desitribution based on the computed parameters for both major and alternative alleles
	MU_A = MIN + ((MAX-MIN)*OPTIM_PAR[1]/(OPTIM_PAR[1]+OPTIM_PAR[2]))
	MU_B = MIN + ((MAX-MIN)*OPTIM_PAR[3]/(OPTIM_PAR[3]+OPTIM_PAR[4]))
	#compute the test statistic alpha
	ALPHA = (MU_A - MU_B)/SD #no penalization W = 2*sqrt(pA*(1-pA))
	OUT = data.frame(ALPHA, allele)
	return(OUT)
}

#END OF FUNCTION DEFINITIONS

#######################################################################################
####################################################################################################################
####################################################################################################################
########################################################################################################################################
##########################################################################################################################################################
########################################################################################################################################
####################################################################################################################
####################################################################################################################
#######################################################################################

# EXECUTION SCRIPT

	# #for testing:
	# model=1
	# n = 500
	# l = 1000
	# m = l
	# q = 10
	# t = q
	# k = 1
	# trait = 1
	# fileNamesSuffix = "Full_Model"
	# iterations = 2

AUC_TWAS = c()
AUC_TWAlpha_1 = c()
AUC_TWAlpha_2 = c()

for (iter in 1:iterations)
{
	#################################
	###							  ###
	### S I M U L A T E   D A T A ###
	###							  ### simulate genomes, transcriptomes and phenotypes
	################################# LD simulated
	SIMULATED = simulate_genomes_transcriptomes_phenotypes(model, n, l, m, q, t, k)
	X = SIMULATED$X
	T = SIMULATED$T
	Y = SIMULATED$Y
	qtl.loc = SIMULATED$qtl.loc
	qtl.additive = SIMULATED$additive

	#################################
	###							  ###
	###         T  W  A  S        ###
	###							  ### individual RNAseq
	################################# individual phenotypes
	p_values = c()
	progressBar = txtProgressBar(min=0, max=l, initial=0, width=50, style=3)
	for (i in 1:l) {
		p_values = c(p_values, summary(lm(Y[,1] ~ T[,i]))$coefficients[2,4])
		setTxtProgressBar(progressBar, i)
	}
	close(progressBar)

	LOD = -log(p_values, base=10)
	LABELS = matrix(0, nrow=l, ncol=1)
	LABELS[qtl.loc,1] = 1
	ROC = simple_roc.bobHorton(LABELS, LOD)
	AUC = simple_auc.erik(ROC$TPR, ROC$FPR)
	AUC_TWAS = c(AUC_TWAS, AUC)

	################################# 1 - PHENOTYPE THEN POOL THEN RNAseq
	###							  ###
	###  P O O L E D    T W A S   ###
	###							  ### pooled RNAseq
	################################# individual phenotypes
	###################
	# (1) PHENOTYPING #
	###################

	#arrange by phenotypic values
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	ALL = ALL[order(ALL$Y), ]
	PHEN = matrix(ALL[, 2])
	GENO = ALL[, (2+1):(2+m)]
	TRAN = ALL[, (2+m+1):(2+m+m)]

	#pooling
	npool = 5
	for (i in 1:npool) { 
		index1 = ((n/npool)*(i-1)) + 1
		index2 = (n/npool)*i
		assign(sprintf("PHEN_pool.%02d", i), PHEN[index1:index2,])
		assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
		assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])
	}
	perc = seq(0, 1, by=1/npool); perc = perc[2:(length(perc)-1)]

	##################
	# (2) GENOTYPING #
	##################
	# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
	SYNC = c()
	for (p in ls()[grepl("GENO_pool.", ls())]) {
		p = eval(parse(text=p))
		sync.A = colSums(p)
		sync.T = (2*nrow(p)) - sync.A
		sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
		sync = apply(sync, 1, paste, collapse=":")
		SYNC = cbind(SYNC, sync)
	}
	SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)

	###############
	# (3) TWAlpha #
	###############
	SD = sqrt(var(PHEN[,1]))
	MIN = min(PHEN[,1])
	MAX = max(PHEN[,1])
	PERC = perc
	Q = quantile(PHEN[,1], perc)

	allele = c()
	alpha = c()
	progressBar = txtProgressBar(min = 0, max = nrow(SYNC), initial = 0, style=3, width=50)
	for (loci in 1:nrow(SYNC)) {
		FREQ_MATRIX = matrix(as.numeric(unlist(strsplit(SYNC[loci,4:ncol(SYNC)], split=":"))), byrow=T, nrow=npool, ncol=6); FREQ_MATRIX = FREQ_MATRIX / rowSums(FREQ_MATRIX)
		out = compute_Alpha(FREQ_MATRIX, SD, MIN, MAX, PERC, Q)
		allele = c(allele, as.character(out$allele))
		alpha = c(alpha, out$ALPHA)
		setTxtProgressBar(progressBar, loci)
	}
	close(progressBar)

	##############
	###		   ###
	### OUTPUT ###
	###		   ###
	##############
	ALPHA = abs(alpha)
	LABELS = matrix(0, nrow=l, ncol=1)
	LABELS[qtl.loc,1] = 1
	ROC = simple_roc.bobHorton(LABELS, ALPHA)
	AUC = simple_auc.erik(ROC$TPR, ROC$FPR)
	AUC_TWAlpha_1 = c(AUC_TWAlpha_1, AUC)
	##############

	################################# 2 - POOL THEN PHENOTYPE THEN RNAseq
	###							  ###
	###  P O O L E D    T W A S   ###
	###							  ### pooled RNAseq
	################################# pooled phenotypes
	###################
	# (1) PHENOTYPING #
	###################
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)

	#pooling ---> let's generate 10 pools
	npool = 10 #even number of pools seem to be better than odd numbers probably due to my unequal partiotioning of pool contamination

	#STEPS:
	#(1) identify 2 pools of 50 individuals each that are highly susceptible and highly resistant then randomize everything in between
		ind.pool = 50
		SUS = ALL[order(ALL$Y),][1:ind.pool, ]
		RES = ALL[order(ALL$Y),][(nrow(ALL)-(ind.pool-1)):nrow(ALL), ]
	#(2) randomize in between
		BET = ALL[order(ALL$Y),][(ind.pool+1):(nrow(ALL)-(ind.pool)), ]; BET = BET[sample(1:nrow(BET), size=nrow(BET), replace=F), ]
	#(3) generate the pools prior to systematic contamination with SUS or RES pools
		ALL2 = rbind(SUS, BET, RES)
		PHEN = matrix(ALL2[, 2])
		GENO = ALL2[, (2+1):(2+m)]
		TRAN = ALL2[, (2+m+1):(2+m+m)]
		for (i in 1:npool) { 
			index1 = ((n/npool)*(i-1)) + 1
			index2 = (n/npool)*i
			assign(sprintf("PHEN_pool.%02d", i), PHEN[index1:index2,])
			assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
			assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])
		}
	#(4) contamination with SUS or RES (KEY STEP IN BULK PHENOTYPING!!!)
		#SUS contamination
			#if npool is even:
			if (npool%%2==0) {
				for (i in 1:((npool-2)/2)) {
					j = i + 1
					k = 1 - (i/((npool-1)/2))
					assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_pool.01[sample(1:length(PHEN_pool.01), size=round(k*length(PHEN_pool.01)), replace=F)]))
					assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_pool.01[sample(1:nrow(GENO_pool.01), size=round(k*nrow(GENO_pool.01)), replace=F), ]))
					assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_pool.01[sample(1:nrow(TRAN_pool.01), size=round(k*nrow(TRAN_pool.01)), replace=F), ]))
				}
			} else {
				#if npool is odd:
				for (i in 1:((npool-1)/2)) {
					#include the center pool (npool-1 instead of npool-2)
					j = i + 1
					k = 1 - (i/((npool-1)/2))
					assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_pool.01[sample(1:length(PHEN_pool.01), size=round(k*length(PHEN_pool.01)), replace=F)]))
					assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_pool.01[sample(1:nrow(GENO_pool.01), size=round(k*nrow(GENO_pool.01)), replace=F), ]))
					assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_pool.01[sample(1:nrow(TRAN_pool.01), size=round(k*nrow(TRAN_pool.01)), replace=F), ]))
				}
			}
		#RES contamination
			#if npool is even:
			if (npool%%2==0) {
				for (i in 1:((npool-2)/2)) {
					j = (npool/2) + i
					k = i/((npool-1)/2)
					assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_pool.10[sample(1:length(PHEN_pool.10), size=round(k*length(PHEN_pool.10)), replace=F)]))
					assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_pool.10[sample(1:nrow(GENO_pool.10), size=round(k*nrow(GENO_pool.10)), replace=F), ]))
					assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_pool.10[sample(1:nrow(TRAN_pool.10), size=round(k*nrow(TRAN_pool.10)), replace=F), ]))
				}
			} else {
				#if npool is odd:
				for (i in 1:((npool-3)/2)) {
					#exclude the center pool (npool-3 instead of npool-2)
					j = ceiling(npool/2) + i
					k = i/((npool-1)/2)
					assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_pool.10[sample(1:length(PHEN_pool.10), size=round(k*length(PHEN_pool.10)), replace=F)]))
					assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_pool.10[sample(1:nrow(GENO_pool.10), size=round(k*nrow(GENO_pool.10)), replace=F), ]))
					assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_pool.10[sample(1:nrow(TRAN_pool.10), size=round(k*nrow(TRAN_pool.10)), replace=F), ]))
				}
			}
	#(4) build distrbution of phenotypic values across pools
		poolMEANS = c()
		poolCUMM = c(0)
		if(trait==0) {
			#trait is quantitative and extracting the mean is effective & efficient
			for (i in ls()[grepl("PHEN_pool.", ls())]) {
				dat = eval(parse(text=i))
				poolMEANS = c(poolMEANS, mean(dat))
				poolCUMM = c(poolCUMM, poolCUMM[length(poolCUMM)]+length(dat))
				#print(i); print(length(dat)); print(mean(dat))
			}
		} else if(trait==1) {
			#trait is binary: e.g. individual plants per pool are either dead or alive - so the mean trait value per pool will be the survival rate
			threshold_for_survival = mean(RES$Y)/2 # set threshold at half of the mean of the resistant pool - that is values lower than this indicates susceptibility
			for (i in ls()[grepl("PHEN_pool.", ls())]) {
				dat = eval(parse(text=i))
				poolMEANS = c(poolMEANS, sum(dat>threshold_for_survival)/length(dat))
				poolCUMM = c(poolCUMM, poolCUMM[length(poolCUMM)]+length(dat))
				#print(i); print(length(dat)); print(mean(dat))
			}
		} else {
			print("Invalid trait state value. Set 0 for quantitative mean and 1 for binary survival rate")
		}
		#plot(density(poolMEANS))
		perc = poolCUMM/max(poolCUMM); perc = perc[2:(length(perc)-1)]

	##################
	# (2) GENOTYPING #
	##################
	# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
	SYNC = c()
	for (p in ls()[grepl("GENO_pool.", ls())]) {
		p = eval(parse(text=p))
		sync.A = colSums(p)
		sync.T = (2*nrow(p)) - sync.A
		sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
		sync = apply(sync, 1, paste, collapse=":")
		SYNC = cbind(SYNC, sync)
	}
	SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)

	###############
	# (3) TWAlpha #
	###############
	SD = sqrt(var(poolMEANS))
	MIN = min(poolMEANS)
	MAX = max(poolMEANS)
	PERC = perc
	Q = quantile(poolMEANS, perc)

	allele = c()
	alpha = c()
	progressBar = txtProgressBar(min = 0, max = nrow(SYNC), initial = 0, style=3, width=50)
	for (loci in 1:nrow(SYNC)) {
		FREQ_MATRIX = matrix(as.numeric(unlist(strsplit(SYNC[loci,4:ncol(SYNC)], split=":"))), byrow=T, nrow=npool, ncol=6); FREQ_MATRIX = FREQ_MATRIX / rowSums(FREQ_MATRIX)
		out = compute_Alpha(FREQ_MATRIX, SD, MIN, MAX, PERC, Q)
		allele = c(allele, as.character(out$allele))
		alpha = c(alpha, out$ALPHA)
		setTxtProgressBar(progressBar, loci)
	}
	close(progressBar)

	##############
	###		   ###
	### OUTPUT ###
	###		   ###
	##############
	ALPHA = abs(alpha)
	LABELS = matrix(0, nrow=l, ncol=1)
	LABELS[qtl.loc,1] = 1
	ROC = simple_roc.bobHorton(LABELS, ALPHA)
	AUC = simple_auc.erik(ROC$TPR, ROC$FPR)
	AUC_TWAlpha_2 = c(AUC_TWAlpha_2, AUC)
	##############

	print(iter)

}

#########################
########################
######################
####################
#  ULTIMATE OUTPUT
####################
######################
########################
#########################

OUT = data.frame(AUC_TWAS, AUC_TWAlpha_1, AUC_TWAlpha_2)
write.csv(OUT, file=paste("OUT_inferFromTRANalone_TWASvsTWAlphas-", fileNamesSuffix, ".csv", sep=""), row.names=F)