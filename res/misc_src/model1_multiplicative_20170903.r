#!/usr/bin/env Rscript
#model1: Y1 = XCBA1 + e; model2: Y2 = XC1 + XCBA1 + e; 
model=2		#1=fully multiplicative; 2=additive-multiplicative
n = 500		#number of individuals
l = 1000 	#number of loci
m = l 		#number of transcripts = number of loci
q = 10		#number of causal loci or QTL (can sampled from some distribution)
t = q 		#number of causal transcripts (can sampled from some distribution)
k = 1		#Verror / Vmodel
additiveGeneEffectsOnly = FALSE
additiveTranscriptEffectsOnly = FALSE
fileNamesSuffix = "Full_Model"

#######
##	 ##
## X ##
##	 ##
#######
#genotype matrix (n x l)
#X = matrix(rbinom(n=(n*l), size=2, prob=0.5), nrow=n, byrow=T) #without LD
freqREF = rbeta(n=l, shape1=2, shape2=2)
freqALT = 1 - freqREF
link = rbinom(n=l, size=1, prob=freqREF)
nCHROM = 7
X1=matrix(NA, nrow=n, ncol=l)
X2=matrix(NA, nrow=n, ncol=l)
progressBar = txtProgressBar(min = 0, max = n, initial = 0, style=3, width=50)
for (ind in 1:n) {
	x1=c()
	x2=c()
	for (i in 1:l) {
		if (i%%(round(n/nCHROM))==1) {
			a1 = sample(c(0, 1), size=1, prob=c(freqREF[i], freqALT[i]))	#No LD between scaffolds
			a2 = sample(c(0, 1), size=1, prob=c(freqREF[i], freqALT[i]))
		} else {
			if (link[i-1]==link[i]) {
				if ((link[i-1]==0 & a1==0) | (link[i-1]==1 & a1==0)) {
					Pab = min(freqREF[i-1], freqREF[i]) / max(freqREF[i-1], freqREF[i])
					a1 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
					a2 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
				} else {
					Pab = min(freqALT[i-1], freqALT[i]) / max(freqALT[i-1], freqALT[i])
					a1 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
					a2 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab)) }
			} else {
				if ((link[i-1]==0 & a1==0) | (link[i-1]==1 & a1==0)) {
					Pab = min(freqREF[i-1], freqALT[i]) / max(freqREF[i-1], freqALT[i])
					a1 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
					a2 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
				} else {
					Pab = min(freqALT[i-1], freqREF[i]) / max(freqALT[i-1], freqREF[i])
					a1 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
					a2 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab)) } 
			}
		}
		x1 = c(x1, a1)
		x2 = c(x2, a2)
	}
	setTxtProgressBar(progressBar, ind)
	X1[ind,] = x1
	X2[ind,] = x2
}
X = X1 + X2

# #plotting ~LD (correlation)
# distance=c()
# LD = c()
# progressBar = txtProgressBar(min = 0, max = ( ((l^2)-l) / 2 ), initial = 0, style=3, width=50); counter = 0
# for (i in 1:(l-1)) {
# 	for (j in (i+1):l) {
# 		distance = c(distance, abs(i-j))
# 		LD = c(LD, cor(X[,i], X[,j]))
# 		setTxtProgressBar(progressBar, counter); counter = counter + 1
# 	}
# }
# jpeg("Pairwise loci correlation - LD measure.jpg", quality=100, width=700, height=500)
# plot(distance, abs(LD))
# dev.off()

# #building G-matrix
# M_matrix = X
# ref.Allele.freq = colSums(X)/(2*nrow(X))
# F_matrix = matrix(rep(ref.Allele.freq, each=n), ncol=l, byrow=F)
# P_matrix = 2*(F_matrix - 0.50)
# Z_matrix = M_matrix - P_matrix
# G_matrix = (Z_matrix %*% t(Z_matrix)) / 2*sum(ref.Allele.freq*(1-ref.Allele.freq))


#######
##	 ##
## C ##
##	 ##
#######
#loci and loci interaction effects (l x l)
	#QTL additive effects sampling parameters:
		#beta distribution:
			qa1=2
			qa2=2
	#QTL interaction effects sampling parameters:
		#beta distribution:
			qi1=1
			qi2=2
	qtl.loc = sample(1:l, size=q, replace=F)
	qtl.additive = matrix(rbeta(n=q, shape1=qa1, shape2=qa2), nrow=q, ncol=1)
	qtl.additive = qtl.additive/sum(qtl.additive)
	c_vect = matrix(0, nrow=l, ncol=1)
	c_vect[qtl.loc,1] = qtl.additive
C = diag(c_vect[,1]) #----> no interactions - completely additive gene effects! - off-diagonals are all zeros!
	if (additiveGeneEffectsOnly == FALSE) {
			qtl.interactions = matrix(rbeta(n=q*q, shape1=qi1, shape2=qi2), nrow=q, ncol=q, byrow=TRUE)
			qtl.interactions = qtl.interactions/sum(qtl.interactions)
			qtl.AxI = qtl.interactions
			diag(qtl.AxI) = qtl.additive
			c_matr = matrix(0, nrow=l, ncol=l)
			c_matr[qtl.loc, qtl.loc] = qtl.AxI
		C = c_matr # ---> additive (diagonals) and interaction (off-diagonals) effects
			#Assesing QTL effects:
				#QTL additive effects distribution
					#plot(density(qtl.additive))
				#QTL interaction effects distribution
					#plot(density(qtl.interactions))
	}
	

#######
##	 ##
## B ##
##	 ##
#######
#genotype to transcript connection (l x m)
	tra.loc = sample(1:m, size=t, replace=F)
	b = matrix(0, nrow=q, ncol=t, byrow=T)
		for (i in seq(1, t)){b[,i]=sample(c(1, rep(0, each=q-1)), size=q, replace=F)}
	B = matrix(0, nrow=l, ncol=m)
B[qtl.loc, tra.loc] = b

#######
##	 ##
## A ##
##	 ##
#######
#transcript (and transcript genotype?!) interactions effects (m x m)
	#transcript additive effects sampling parameters:
		#beta distribution:
			ta1=2
			ta2=2
	#transcript interaction effects sampling parameters:
		#beta distribution:
			ti1=0.25
			ti2=1
	tra.additive = matrix(rbeta(n=t, shape1=ta1, shape2=ta2), nrow=t, ncol=1)
	tra.additive = tra.additive/sum(tra.additive)
	a_vect = matrix(0, nrow=m, ncol=1)
	a_vect[tra.loc,1] = tra.additive
A = diag(a_vect[,1]) #----> no interactions - completely additive transcript effects! - off-diagonals are all zeros!
	if (additiveTranscriptEffectsOnly == FALSE) {
			tra.interactions = matrix(rbeta(n=q*q, shape1=ti1, shape2=ti2), nrow=q, ncol=q, byrow=TRUE)
			tra.interactions = tra.interactions/sum(tra.interactions)
			tra.AxI = tra.interactions
			diag(tra.AxI) = tra.additive
			a_matr = matrix(0, nrow=m, ncol=m)
			a_matr[tra.loc, tra.loc] = tra.AxI
		A = a_matr # ---> additive (diagonals) and interaction (off-diagonals) effects
			#Assesing tra effects:
				#tra additive effects distribution
					#plot(density(tra.additive))
				#tra interaction effects distribution
					#plot(density(tra.interactions))
	}


#######
##	 ##
## J ##
##	 ##
#######
#vector of ones for summing up all effects per individual
j = matrix(1, nrow=m, ncol=1)

#######
##	 ##
## e ##
##	 ##
#######
#residual effects ~ N(0, sd=??? --> such that h2=50%)
if (model == 1) {
Vmodel = var(X %*% C %*% B %*% A %*% j)
Verror = k*Vmodel
e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
} else if (model == 2) {
Vmodel = var((X %*% C %*% j) + (X %*% C %*% B %*% A %*% j))
Verror = k*Vmodel
e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
}

#######
##	 ##
## Y ##
##	 ##
#######
#phenotypes
if (model == 1) {
#fully multiplicative
Y = (X %*% C %*% B %*% A %*% j) + e
} else if (model ==2){
#additive-multiplicative?
Y = (X %*% C %*% j) + (X %*% C %*% B %*% A %*% j) + e
}

#######
##	 ##
## T ##
##	 ##
#######
#transcript abundance data - all transcripts
t_causal = X %*% C %*% B #scaled abundance matrix of causal transcripts
T = matrix(rbeta(n=n*m, shape1=0.5, shape2=2), nrow=n, ncol=m, byrow=T)
T[,tra.loc] = t_causal[,tra.loc]



################################################################################################################################################################
################################################################################################################################################################
################################################################################################################################################################



###################################################################################################
###																								###
###		Inferring parameters (or effects) given phenotypes from the different models built		###
###																								###
###################################################################################################

#METHODS:
	#(1) Least squares approach - minimize error variance
	#(2) Maximum likelihood approach - maximize likelihood of parameters given data
	#(3) Bayesian approach - convergence of posterior = likelihood x prior / integral(likelihood x prior)

#####################
#
#(1) LEAST SQUARES: fit each gene, transcript and/or gene-transcript combinations into some specific model or a combination of models
#						then minimize the residual variance
#####################
	simple_roc.bobHorton <- function(labels, scores){labels <- labels[order(scores, decreasing=TRUE)]; data.frame(TPR=cumsum(labels)/sum(labels), FPR=cumsum(!labels)/sum(!labels), labels)}
	simple_auc.erik <- function(TPR, FPR){mean(sample(TPR,l,replace=TRUE) > sample(FPR,l,replace=TRUE))}
	############## 1.1/3
	# SNPs ALONE #
	##############
	p_values = c()
	progressBar = txtProgressBar(min=0, max=l, initial=0, width=50, style=3)
	for (i in 1:l) {
		p_values = c(p_values, summary(lm(Y[,1] ~ X[,i]))$coefficients[2,4])
		setTxtProgressBar(progressBar, i)
	}
	close(progressBar)
	LOD = -log(p_values, base=10)
	LABELS = matrix(0, nrow=l, ncol=1)
	LABELS[qtl.loc,1] = 1
	ROC = simple_roc.bobHorton(LABELS, LOD)
	AUC = simple_auc.erik(ROC$TPR, ROC$FPR)
	#manhattan plot
	jpeg(paste("ManhattanPlot-GWAS-" , fileNamesSuffix, ".jpeg", sep=""), quality=100, width=700, height=500)
	plot(0,pch='',xlim=c(0,l),xlab="SNP ID",xaxt='n',ylab=expression(-log[10](italic(p))),ylim=c(0,max(LOD,na.rm=T)),bty="n",las=2,main="Manhattan Plot - GWAS (GLM)")
	points(LOD)
	dev.off()
	#roc plot and auc
	jpeg(paste("ROCPlot-GWAS-", fileNamesSuffix, ".jpeg", sep=""), quality=100, width=500, height=500)
	plot(x=ROC$FPR, y=ROC$TPR, xlab="False Positive Rate", ylab="True Positive Rate", main="ROC Plot - GWAS (GLM)")
	lines(x=ROC$FPR, y=ROC$TPR)
	abline(0,1, lty=2)
	legend("bottomright",legend=paste("AUC = ", AUC, sep=""), bty ="n", pch=NA)
	dev.off()

	#plot(LABELS, LOD)

	# #EXTRA! EXTRA! Using Fst to infer QTL:
	# n.sub.popns = 100
	# ordered.X = X[order(Y[,1]),]
	# Fst = c()
	# progressBar = txtProgressBar(min=0, max=l, initial=0, width=50, style=3)
	# for (i in 1:l) {
	# 	freqs = c()
	# 	for (j in 1:n.sub.popns) {
	# 		freqs = c(freqs, mean(X[(((j-1)*(n/n.sub.popns))+1):(j*(n/n.sub.popns)),i]) / 2)
	# 	}
	# 	Ht = 1 - ( (mean(freqs)^2) + (mean(1-freqs)^2) )
	# 	Hs = mean( 1 - ((freqs)^2 + ((1-freqs)^2)) )
	# 	Fst = c(Fst, (Ht-Hs)/Ht)
	# 	setTxtProgressBar(progressBar, i)
	# }
	# close(progressBar)
	# plot(LOD, Fst)
	# plot(LABELS, Fst)


	#################### 1.2/3
	# TRANSCRIPT ALONE #
	####################
	p_values = c()
	progressBar = txtProgressBar(min=0, max=m, initial=0, width=50, style=3)
	for (i in seq(1, m)) {
		p_values = c(p_values, summary(lm(Y[,1] ~ T[,i]))$coefficients[2,4])
		setTxtProgressBar(progressBar, i)
	}
	close(progressBar)
	LOD = -log(p_values, base=10)
	LABELS = matrix(0, nrow=l, ncol=1)
	LABELS[tra.loc,1] = 1
	ROC = simple_roc.bobHorton(LABELS, LOD)
	AUC = simple_auc.erik(ROC$TPR, ROC$FPR)
	#manhattan plot
	jpeg(paste("ManhattanPlot-TWAS-" , fileNamesSuffix, ".jpeg", sep=""), quality=100, width=700, height=500)
	plot(0,pch='',xlim=c(0,m),xlab="Transcript ID",xaxt='n',ylab=expression(-log[10](italic(p))),ylim=c(0,max(LOD,na.rm=T)),bty="n",las=2,main="Manhattan Plot - TWAS (GLM)")
	points(LOD)
	dev.off()
	#roc plot and auc
	jpeg(paste("ROCPlot-TWAS-", fileNamesSuffix, ".jpeg", sep=""), quality=100, width=500, height=500)
	plot(x=ROC$FPR, y=ROC$TPR, xlab="False Positive Rate", ylab="True Positive Rate", main="ROC Plot - TWAS (GLM)")
	lines(x=ROC$FPR, y=ROC$TPR)
	abline(0,1, lty=2)
	legend("bottomright",legend=paste("AUC = ", AUC, sep=""), bty ="n", pch=NA)
	dev.off()

	################################### 1.3/3
	# SNP & TRANSCRIPT ADDITIVE LASSO #
	###################################
	library(glmnet)
	# glmnet(x, y, family=c("gaussian","binomial","poisson","multinomial","cox","mgaussian"),
	# 	weights, offset=NULL, alpha = 1, nlambda = 100,
	# 	lambda.min.ratio = ifelse(nobs<nvars,0.01,0.0001), lambda=NULL,
	# 	standardize = TRUE, intercept=TRUE, thresh = 1e-07, dfmax = nvars + 1,
	# 	pmax = min(dfmax * 2+20, nvars), exclude, penalty.factor = rep(1, nvars),
	# 	lower.limits=-Inf, upper.limits=Inf, maxit=100000,
	# 	type.gaussian=ifelse(nvars<500,"covariance","naive"),
	# 	type.logistic=c("Newton","modified.Newton"),
	# 	standardize.response=FALSE, type.multinomial=c("ungrouped","grouped"))
	LASSO = cv.glmnet(x=cbind(X,T), y=Y[,1], family='gaussian', alpha=1)
	LASSO_OUT = matrix(coef(LASSO, s='lambda.min')[2:length(coef(LASSO, s='lambda.min')),], nrow=l+m, ncol=1)
	LASSO_snp = LASSO_OUT[1:l, 1]
	LASSO_tra = LASSO_OUT[(l+1):(l+m), 1]

	LABELS_snp = matrix(0, nrow=l, ncol=1)
	LABELS_snp[qtl.loc,1] = 1
	ROC_snp = simple_roc.bobHorton(LABELS_snp, LASSO_snp)
	AUC_snp = simple_auc.erik(ROC_snp$TPR, ROC_snp$FPR)

	LABELS_tra = matrix(0, nrow=m, ncol=1)
	LABELS_tra[tra.loc,1] = 1
	ROC_tra = simple_roc.bobHorton(LABELS_tra, LASSO_tra)
	AUC_tra = simple_auc.erik(ROC_tra$TPR, ROC_tra$FPR)

	#SNPs and Transcrips Effects plots
	jpeg(paste("SNP_effects-LASSO-" , fileNamesSuffix, ".jpeg", sep=""), quality=100, width=700, height=500)
	plot(0,pch='',xlim=c(0,l),xlab="SNP ID",xaxt='n',ylab="LASSO beta",ylim=c(min(LASSO_snp,na.rm=T),max(LASSO_snp,na.rm=T)),bty="n",las=2,main="SNP Effects - LASSO (SNPs + Transcripts)")
	points(LASSO_snp)
	dev.off()

	jpeg(paste("Transcript_effects-LASSO-" , fileNamesSuffix, ".jpeg", sep=""), quality=100, width=700, height=500)
	plot(0,pch='',xlim=c(0,m),xlab="Transcript ID",xaxt='n',ylab="LASSO beta",ylim=c(min(LASSO_tra,na.rm=T),max(LASSO_tra,na.rm=T)),bty="n",las=2,main="Transcript Effects - LASSO (SNPs + Transcripts)")
	points(LASSO_tra)
	dev.off()

	#roc plot and auc
	jpeg(paste("ROCPlot-LASSO-SNPs-", fileNamesSuffix, ".jpeg", sep=""), quality=100, width=500, height=500)
	plot(x=ROC_snp$FPR, y=ROC_snp$TPR, xlab="False Positive Rate", ylab="True Positive Rate", main="ROC Plot - LASSO (X+T): SNPs")
	lines(x=ROC_snp$FPR, y=ROC_snp$TPR)
	abline(0,1, lty=2)
	legend("bottomright",legend=paste("AUC = ", AUC_snp, sep=""), bty ="n", pch=NA)
	dev.off()

	jpeg(paste("ROCPlot-LASSO-Transcripts-", fileNamesSuffix, ".jpeg", sep=""), quality=100, width=500, height=500)
	plot(x=ROC_tra$FPR, y=ROC_tra$TPR, xlab="False Positive Rate", ylab="True Positive Rate", main="ROC Plot - LASSO (X+T): Transcripts")
	lines(x=ROC_tra$FPR, y=ROC_tra$TPR)
	abline(0,1, lty=2)
	legend("bottomright",legend=paste("AUC = ", AUC_tra, sep=""), bty ="n", pch=NA)
	dev.off()



#############################
#
#(2) MAXIMUM LIKELIHOOD: fit each gene, transcript and/or gene-transcript combinations into some specific model or a combination of models
#							then find the parameter/s that maximize/s the log likelihood of the data
#############################

#test code (from alex 20170907):
P_obs=rnorm(1000, mean=10, sd=2)
loglik <- function(par, y) {sum(dnorm(y, mean=par[1], sd=sqrt(par[2]), log = TRUE))}
Par=optim(par=c(mean = 0, var = 1), fn = loglik, y=P_obs, control=list(fnscale = -1, reltol = 1e-16))$par

P_obs=rbinom(100,1,prob=.20)
loglik <- function(prob, y) {sum(dbinom(y, 1,prob=prob, log = TRUE))}
Par=optim(par = c(prob=.5), fn = loglik,y=P_obs, control = list(fnscale = -1, reltol = 1e-5),method="Brent",lower=0,upper=1)$par


#############################
#
#(3) BAYESIAN ESTIMATION: fit each gene, transcript and/or gene-transcript combinations into some specific model or a combination of models
#							then find the parameters whose posterior probability is maximized (converges) after many iterations through the parameter space
#############################
