wget -r --no-parent http://1001genomes.org/data/Salk/releases/2013_24_01/
#then download the transcriptomes from https://www.ncbi.nlm.nih.gov/geo/download/GSE43858_RAW.tar #cannot wget :-(
#scp to server
#rename to GENOME and TRANSCRIPTOME folders

#############################
###						  ###
### TRANSCRIPTOME MERGING ###
###						  ###
#############################
cd TRANSCRIPTOME
ls | grep .tsv  > folders.list
ls | grep .tsv | cut -d_ -f2,3 | sed 's/_expression.tsv//g' | paste -s > samples.list

for file in $(cat folders.list)
do
cut -d$'\t' -f3 ${file}/data | tail -n+2 > FPKM.${file}.temp
done

paste -d$'\t' FPKM* > merged.temp
cat samples.list merged.temp > merged2.temp
cut -d$'\t' -f1,2 $(head -n1 folders.list)/data > column_labels.temp
paste -d$'\t' column_labels.temp merged2.temp > MERGED.FPKM

rm *.temp *.list

# #checking symmetry:
# head -n1 MERGED.FPKM | wc -w
# head -n$(cat MERGED.FPKM | wc -l) MERGED.FPKM | tail -n1 | wc -w
# wc -l MERGED.FPKM
# cut -d$'\t' -f1 MERGED.FPKM | wc -l
# cut -d$'\t' -f100 MERGED.FPKM | wc -l
# cut -d$'\t' -f$(head -n1 MERGED.FPKM | wc -w) MERGED.FPKM | wc -l

#listing common samples with genomes and transcriptomes available
cd ..
ls TRANSCRIPTOMES | grep .tsv | cut -d_ -f2,3 | sed 's/_expression.tsv//g' > transcript.samples
ls GENOMES > genomes.samples
cat transcript.samples genomes.samples | sort -k1 | uniq -d > common_samples.list
rm *.samples

######################
###				   ###
### GENOME MERGING ### (vcf conversion then bcftool merge)
###				   ###
######################
cd GENOMES
gzip -d */*.gz

for sample in $(cat ../common_samples.list)
do

echo $sample

echo -e "##fileformat=VCFv4.0" > ${sample}.vcf
echo -e "##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of Samples With Data">" >> ${sample}.vcf
echo -e "##INFO=<ID=DP,Number=1,Type=Integer,Description="Total Depth">" >> ${sample}.vcf
echo -e "##FILTER=<ID=q25,Description="Quality below 25">" >> ${sample}.vcf
echo -e "##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">" >> ${sample}.vcf
echo -e "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Genotype Quality">" >> ${sample}.vcf
echo -e "##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Read Depth">" >> ${sample}.vcf
echo -e "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t$sample" >> ${sample}.vcf

cut -d$'\t' -f1 ${sample}/quality_variant_filtered_${sample}.txt | sed 's/.*/./g' > dots.temp
cut -d$'\t' -f1 ${sample}/quality_variant_filtered_${sample}.txt | sed 's/.*/GT/g' > format.temp
cut -d$'\t' -f1 ${sample}/quality_variant_filtered_${sample}.txt | sed 's/.*/1\/1/g' > samples_col.temp
cut -d$'\t' -f2,3 ${sample}/quality_variant_filtered_${sample}.txt > chrom_pos.temp
cut -d$'\t' -f4,5,6 ${sample}/quality_variant_filtered_${sample}.txt > ref_alt_qual.temp
paste -d$'\t' chrom_pos.temp dots.temp ref_alt_qual.temp dots.temp dots.temp format.temp samples_col.temp >> ${sample}.vcf
bgzip -c ${sample}.vcf > ${sample}.vcf.gz
bcftools index ${sample}.vcf.gz
rm *.temp ${sample}.vcf
done

bcftools merge *.vcf.gz > MERGED.temp
#converting missing SNPs as the reference allele! --> Is this valid??!!
sed 's/.\/./0\/0/g' MERGED.temp > MERGED2.temp
vcftools --recode --vcf MERGED.temp --max-missing-count 30 --max-alleles 3 --min-alleles 3 --out MERGED
mv MERGED.recode.vcf MERGED.vcf
rm *.vcf.gz* *.temp *.log

########################
#######################
######################
#####################
####################
###################
##################
#################
################  NOOOO!!!! Salk SNP data may be in adequate... Downloading 10001 Genomes' 1135 VCF genotype data including Salk's
###############
##############
#############
############
###########
##########

############################
###						 ###
### DUMMY NEURAL NETWORK ### 
###						 ###
############################
#Rscript:
nINDs = 75
nSNPs = 100
nTRANs = 50
GENO = matrix(rbinom(n=nSNPs*nINDs, size=1, prob=0.5), nrow=nINDs, ncol=nSNPs)
	geno2trans.transform = matrix(rnorm(nSNPs*nTRANs), nrow=nSNPs) * matrix(rbinom(nSNPs*nTRANs, 1, p=0.50), nrow=nSNPs)
TRAN = GENO %*% geno2trans.transform

# # hidden layer count = 1
# nINPUTs = nSNPs
# INPUT = GENO[,1]
# nNODEs = round(nSNPs*0.5)
# CONNECTIONS1 = matrix(sample(x=seq(0,1, by=0.001), size=nSNPs*nTRANs, replace=TRUE), nrow=nNODEs, ncol=nINPUTs)
# #ACTIVATIONS1 = 1/(1+exp(-CONNECTIONS1 %*% INPUT)) #sigmoid
# ACTIVATIONS1 = (1-exp(-2*(CONNECTIONS1 %*% INPUT))) / (1+exp(2*(CONNECTIONS1 %*% INPUT))) #hyperbolic tangent
# ACTIVATIONS1 = log(1+exp(CONNECTIONS1 %*% INPUT)) #softplus

#using R-built-in nnet package
library(nnet)

GENO.train = GENO[1:round(nINDs*0.5),]
TRAN.train = TRAN[1:round(nINDs*0.5),]
GENO.valid = GENO[(round(nINDs*0.5)+1):nINDs,]
TRAN.valid = TRAN[(round(nINDs*0.5)+1):nINDs,]

test1 = nnet(x=GENO.train, y=TRAN.train[,1], size=5)
summary(test1)
