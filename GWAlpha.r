#Rscript source function: synthethic pooled GWAS or GWAlpha using maximum likelihood estimation

# INPUTS:
#1	SYNC -->	matrix of allele freqencies across pools (sync file format: nloci x npools x 6 allele states: A,T,C,G,N,DEL)
#2	SD -->		standard deviation of phenotype data across all pools
#3	MIN -->		minimum phenotypic values across all pools
#4	MAX -->		maximum phenotypic values across all pools
#5	PERC -->	list of percentile values, e.g. c(0.2, 0.4, 0.6, 0.8) for 5 pools (1 x (npools-1))

# OUTPUT:
#1	ALPHA -->	list of test statistic alpha across all loci or transcripts (1xm)

# compute alpha for each loci:
compute_SNP_Alpha <- function(FREQ_MATRIX, SD, MIN, MAX, PERC) {
	#determine the size of each pool based on their percentile values
	BINS = c(PERC, 1) - c(0, PERC)
	#find the major allele - that is the allele with the highest frequency across pools
	allele_means = colMeans(FREQ_MATRIX)
	allele_max_index = which.max(colSums(FREQ_MATRIX)); if(allele_max_index==1){allele="A"} else if(allele_max_index==2){allele="T"} else if(allele_max_index==3){allele="C"} else if(allele_max_index==4){allele="G"} else if(allele_max_index==5){allele="N"} else {allele="DEL"} #determine major allele identity
	FREQ_A = FREQ_MATRIX[,allele_max_index]	#potential problem here when more than 1 column or allele are equal to the maximum value!!!
	#transform the allele frequency of the major allele such that the sum of allele frequencies across pools is equal to 1
	BIN_A = FREQ_A*BINS/sum(FREQ_A*BINS); BIN_A = BIN_A/sum(BIN_A) #is dividing them again with the sum necessary???
	#transform the aggregate of the alternative alleles in the same way as in the major allele
	BIN_B = (1 - FREQ_A)*BINS/(1-sum(FREQ_A*BINS));	BIN_B = BIN_B/sum(BIN_B)
	#convert back the transformed allele frequencies into percentiles
	PERC_A = c(); PERC_B = c()
	for(i in 1:length(BIN_A)){
		PERC_A = c(PERC_A, sum(BIN_A[1:i]))
		PERC_B = c(PERC_B, sum(BIN_B[1:i]))
	}
	#define the likelihood function as the cdf(yi, b1,b2) - cdf(yi-1, b1,b2) [--> for the major allele] PLUS cdf(yi, b3,b4) - cdf(yi-1, b3,b4) [--> for the altenative alleles aggregate]
	optimize_me <- function(PERC_A, PERC_B, par) {
		PERC_A0 = c(0, PERC_A[1:(length(PERC_A)-1)])
		PERC_B0 = c(0, PERC_B[1:(length(PERC_B)-1)])
		return(-sum(log(pbeta(PERC_A, shape1=par[1], shape2=par[2])-pbeta(PERC_A0, shape1=par[1], shape2=par[2]))) - sum(log(pbeta(PERC_B, shape1=par[3], shape2=par[4])-pbeta(PERC_B0, shape1=par[3], shape2=par[4]))) )
	}
	#maximize the likelihood or in the case minimize the -log likelihood
	OPTIM_PAR = tryCatch(optim(par=c(1,1,1,1), fn=optimize_me, control=list(reltol=10e-8), PERC_A=PERC_A, PERC_B=PERC_B)$par, error=function(e){return(c(1,1,1,1))})
	#compute for the mean of the beta desitribution based on the computed parameters for both major and alternative alleles
	MU_A = MIN + ((MAX-MIN)*OPTIM_PAR[1]/(OPTIM_PAR[1]+OPTIM_PAR[2]))
	MU_B = MIN + ((MAX-MIN)*OPTIM_PAR[3]/(OPTIM_PAR[3]+OPTIM_PAR[4]))
	#compute the test statistic alpha
	ALPHA = (MU_A - MU_B)/SD #no penalization W = 2*sqrt(pA*(1-pA))
	OUT = data.frame(ALPHA, allele)
	return(OUT)
}


#compute alpha across all loci by calling the previous function: "compute_SNP_Alpha"
GWAlpha <- function(SYNC, SD, MIN, MAX, PERC) {
	allele = c()
	alpha = c()
	progressBar = txtProgressBar(min = 0, max = nrow(SYNC), initial = 0, style=3, width=50)
	for (loci in 1:nrow(SYNC)) {
		FREQ_MATRIX = matrix(as.numeric(unlist(strsplit(SYNC[loci,4:ncol(SYNC)], split=":"))), byrow=T, nrow=npool, ncol=6); FREQ_MATRIX = FREQ_MATRIX / rowSums(FREQ_MATRIX)
		out = compute_SNP_Alpha(FREQ_MATRIX, SD, MIN, MAX, PERC)
		allele = c(allele, as.character(out$allele))
		alpha = c(alpha, out$ALPHA)
		setTxtProgressBar(progressBar, loci)
	}
	close(progressBar)
	ALPHA = abs(alpha)
	return(ALPHA)
}