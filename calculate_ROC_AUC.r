#Rscript source function: ROC and AUC calculation from LOD or alpha

# INPUTS for ROC function:
#1	labels -->		list of 1's and 0's - incidence list (1xm)
#2	scores -->		LOD or |alpha| - test statistic (1xm)

# OUTPUTS for ROC function and INPUTS for AUC function:
#1	TPR -->			true positive rate or sensitivity (1xm)
#2	FPR -->			false positive rate or 1-specificity (1xm)

# OUTPUT for AUC function:
#1	AUC -->			area under the ROC plot

simple_roc.bobHorton <- function(labels, scores){
	labels <- labels[order(scores, decreasing=TRUE)]
	out = data.frame(TPR=cumsum(labels)/sum(labels), FPR=cumsum(!labels)/sum(!labels), labels)
	return(out)
}

simple_auc.erik <- function(TPR, FPR, iter=10000){
	AUC = c()
	for (i in 1:iter) {
		AUC = c(AUC, mean(sample(TPR,length(TPR),replace=TRUE) > sample(FPR,length(FPR),replace=TRUE)))
	}
	return(mean(AUC))
}

simple_auc.jef <-function(TPR, FPR) {
	x1 = FPR[1:(length(FPR)-1)]
	x2 = FPR[2:length(FPR)]
	dx = x2-x1
	y1 = TPR[1:(length(TPR)-1)]
	
	AUC = sum(dx*y1)
	return(AUC)
}

# library(ROCR)
# pr = prediction(LOD_GWAS, qtl.labels)
# prf = performance(pr, measure="tpr", x.measure="fpr")
# auc = performance(pr, measure="auc")@y.values[[1]]
# plot(prf, col="red", main="ROC Plot", sub=paste("AUC = ", round(auc,2), sep=""))