A = seq(0,1, by=0.1)
B = seq(0,1, by=0.1)
l = seq(0,1, by=0.1)

PAB=c();PA=c();PB=c();Pl=c()
bar = txtProgressBar(min=0, max=length(A)*length(B)*length(l), style=3)
for (a in 1:length(A)){
	for(b in 1:length(B)){
		for(i in 1:length(l)){
			PAB = c(PAB, ((1-l[i])*A[a]*B[b])+(l[i]*(min(A[a],B[b])/max(A[a],B[b]))))
			PA=c(PA,A[a]);PB=c(PB,B[b]);Pl=c(Pl, l[i])
			setTxtProgressBar(bar, length(PAB))
		}
	}
}

OUT = data.frame(as.factor(PA, PB, Pl, PAB)

jpeg("Linkage disequilibrium properties of made-up parameter.jpeg", quality=100, width=1000, height=1000)
par(mfrow=c(2,2), cex.lab=2, cex.axis=2, mar=c(7,6,1,1))
boxplot(PAB~PA, ylab="P(AB)", xlab="P(A)")
boxplot(PAB~PB, ylab="P(AB)", xlab="P(B)")
boxplot(PAB~Pl, ylab="P(AB)", xlab="lambda")
plot(density(PAB[!is.na(PAB)]))
dev.off()
