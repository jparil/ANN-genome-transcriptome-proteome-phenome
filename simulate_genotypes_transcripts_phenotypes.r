#Rscript source function: simulate genomes (genotypes: 0,1,2), transcriptomes (transcript abundance data: 0-nK) and phenotypes

###############
####
#### NOTE: I have to connect causal transcript location with QTL!!! 20180111
####
##############


# INPUTS:
#1	model -->			1=fully multiplicative; 2=additive-multiplicative
#2	n --> 				number of individuals
#3	l -->				number of loci
#4	m -->				number of transcripts = number of loci
#5	q -->				number of causal loci or QTL (can sampled from some distribution)
#6	t -->				number of causal transcripts (can sampled from some distribution)
#7	k -->				Verror / Vmodel

# OUTPUTS:
#1	X -->				matrix of genotype data (0,1,2) (nxl)
#2	T -->				matrix of transcript data (R) (nxm)
#3	qtl.loc -->			list of locations of the causal SNP (1xq)
#4	qtl.additive -->	list of addditive effects of the causal SNP (1xq)
#5	tra.loc -->			list of locations of the causal transcripts (1xt)
#6	tra.additive -->	list of addditive effects of the causal transcripts (1xt)

# #for testing:
	# model=1
	# n = 500
	# l = 1000
	# m = l
	# q = 10
	# t = q
	# k = 1

simulate_genomes_transcriptomes_phenotypes <- function(model, n, l, m, q, t, k) {
	additiveGeneEffectsOnly = FALSE
	additiveTranscriptEffectsOnly = FALSE

	# #test:
	# model=1
	# n=500
	# l=1000
	# m=l
	# q=20
	# t=q
	# k=0.8

	#######
	##	 ##
	## X ##
	##	 ##
	#######
	# #genotype matrix (n x l)
	# X = matrix(rbinom(n=(n*l), size=2, prob=0.5), nrow=n, byrow=T) #without LD
	# freqREF = rbeta(n=l, shape1=2, shape2=2)
	# freqALT = 1 - freqREF
	# link = rbinom(n=l, size=1, prob=freqREF)
	# lambda = 0 #maximum linkage (lambda=0 for no linkage)
	# nCHROM = 7
	# X1=matrix(NA, nrow=n, ncol=l)
	# X2=matrix(NA, nrow=n, ncol=l)
	# progressBar = txtProgressBar(min = 0, max = n, initial = 0, style=3, width=50)
	# for (ind in 1:n) {
	# 	x1=c()
	# 	x2=c()
	# 	for (i in 1:l) {
	# 		if (i%%(round(n/nCHROM))==1) {
	# 			a1 = sample(c(0, 1), size=1, prob=c(freqREF[i], freqALT[i]))	#No LD between scaffolds or chromosomes
	# 			a2 = sample(c(0, 1), size=1, prob=c(freqREF[i], freqALT[i]))
	# 		} else {
	# 			if (link[i-1]==link[i]) {
	# 				if ((link[i-1]==0 & a1==0) | (link[i-1]==1 & a1==0)) {
	# 					Pab = ((1-lambda)*freqREF[i-1]*freqREF[i]) + (lambda*(min(freqREF[i-1], freqREF[i]) / max(freqREF[i-1], freqREF[i])))
	# 					a1 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
	# 					a2 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
	# 				} else {
	# 					Pab = ((1-lambda)*freqALT[i-1]*freqALT[i]) + (lambda*(min(freqALT[i-1], freqALT[i]) / max(freqALT[i-1], freqALT[i])))
	# 					a1 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
	# 					a2 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab)) }
	# 			} else {
	# 				if ((link[i-1]==0 & a1==0) | (link[i-1]==1 & a1==0)) {
	# 					Pab = ((1-lambda)*freqREF[i-1]*freqALT[i]) + (lambda*(min(freqREF[i-1], freqALT[i]) / max(freqREF[i-1], freqALT[i])))
	# 					a1 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
	# 					a2 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
	# 				} else {
	# 					Pab = ((1-lambda)*freqALT[i-1]*freqREF[i]) + (lambda*(min(freqALT[i-1], freqREF[i]) / max(freqALT[i-1], freqREF[i])))
	# 					a1 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
	# 					a2 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab)) } 
	# 			}
	# 		}
	# 		x1 = c(x1, a1)
	# 		x2 = c(x2, a2)
	# 	}
	# 	setTxtProgressBar(progressBar, ind)
	# 	X1[ind,] = x1
	# 	X2[ind,] = x2
	# }
	# close(progressBar)
	# X = X1 + X2

	#alternative simulation of linkage using multivariate normal distribution
	library(MASS)
	freqREF = rbeta(n=l, shape1=2, shape2=2)
	sim_gen <- function(freqREF){
		X = matrix(rbinom(n=(n*l), size=2, prob=freqREF), nrow=n, byrow=T) #without LD
		mu=freqREF
		decay.point = 20 #distance in terms of how many markers away do expect substantial linkage to occur... needs a lot of fixing
		sigma=var(X) #define the variance covariance matrix initially
		max.sigma=max(sigma)
		#simulate linkage between proximal markers up to the maximum decay.point
		for (i in 1:l){
			for (j in 1:l){
				if(i==j-1 | j==i-1) {sigma[i,j] = max.sigma
				} else if(i==(j-sample(1:decay.point, size=1)) | j==(i-sample(1:decay.point, size=1))) {sigma[i,j] = max.sigma} #adds stochasticity
			}
		}
		sigma = t(sigma)%*%sigma #converting sigma with large pairwise correlations to simulate linkage into a perfectly posistive semi-deifinite matrix i.e. sigma = A'A
		X = mvrnorm(n=n, mu=mu, Sigma=sigma)
		X = round( (X-min(X)) / (max(X)-min(X)) )
		return(X)
	}
	X1=sim_gen(freqREF)
	X2=sim_gen(freqREF)
	X = X1+X2
	
	# #plotting ~LD (correlation)
	distance=c()
	LD = c()
	X.sub = X[,1:(l/10)]
	l.sub = ncol(X.sub)
	progressBar = txtProgressBar(min = 0, max = ( ((l.sub^2)-l.sub) / 2 ), initial = 0, style=3, width=50); counter = 0
	for (i in 1:(l.sub-1)) {
		for (j in (i+1):l.sub) {
			distance = c(distance, abs(i-j))
			LD = c(LD, cor(X.sub[,i], X.sub[,j]))
			setTxtProgressBar(progressBar, counter); counter = counter + 1
		}
	}
	jpeg("Pairwise loci correlation - LD measure.jpg", quality=100, width=700, height=500)
	plot(distance, abs(LD))
	dev.off()

	#building G-matrix
	M_matrix = X
	ref.Allele.freq = colSums(X)/(2*nrow(X))
	F_matrix = matrix(rep(ref.Allele.freq, each=n), ncol=l, byrow=F)
	P_matrix = 2*(F_matrix - 0.50)
	Z_matrix = M_matrix - P_matrix
	G_matrix = (Z_matrix %*% t(Z_matrix)) / 2*sum(ref.Allele.freq*(1-ref.Allele.freq))


	#######
	##	 ##
	## K ##
	##	 ##
	#######
	#loci and loci interaction effects (l x l)
		#QTL additive effects sampling parameters:
			# #beta distribution:
			# 	qa1=2
			# 	qa2=2
			#normal distribution:
				qa1=0 #mean additive qlt effect
				qa2=4 #additive qtl variance (maybe add another parameter as the ratio between additive and non-additive variance?!)
		#QTL interaction effects sampling parameters:
			# #beta distribution:
			# 	qi1=1
			# 	qi2=2
			#normal distribution:
				qi1=0 #mean non-additive qtl effect
				qi2=1 #non-additive qtl variance
		qtl.loc = sample(1:l, size=q, replace=F)
		#qtl.additive = matrix(rbeta(n=q, shape1=qa1, shape2=qa2), nrow=q, ncol=1)
		#qtl.additive = qtl.additive/sum(qtl.additive)
		qtl.additive = matrix(rnorm(n=q, mean=qa1, sd=sqrt(qa2)), nrow=q, ncol=1)
		k_vect = matrix(0, nrow=l, ncol=1)
		k_vect[qtl.loc,1] = qtl.additive
	K = diag(k_vect[,1]) #----> no interactions - completely additive gene effects! - off-diagonals are all zeros!
		if (additiveGeneEffectsOnly == FALSE) {
				#qtl.interactions = matrix(rbeta(n=q*q, shape1=qi1, shape2=qi2), nrow=q, ncol=q, byrow=TRUE)
				#qtl.interactions = qtl.interactions/sum(qtl.interactions)
				qtl.interactions = matrix(rnorm(n=q*q, mean=qi1, sd=sqrt(qi2)), nrow=q, ncol=q, byrow=TRUE)
				qtl.AxI = qtl.interactions
				diag(qtl.AxI) = qtl.additive
				k_matr = matrix(0, nrow=l, ncol=l)
				k_matr[qtl.loc, qtl.loc] = qtl.AxI
			K = k_matr # ---> additive (diagonals) and interaction (off-diagonals) effects
		}
	# #assesing qtl effects distributions:
	# plot(density(K[K!=0])) #whole matrix
	# plot(density(diag(K)[diag(K)!=0])) #addtive effects only
	# d = row(K) - col(K)
	# plot(density(K[d!=0][K[d!=0]!=0])) #all non-additive effects

	#######
	##	 ##
	## A ##
	##	 ##
	#######
	#genotype to transcript connection (l x m)
		tra.loc = sample(1:m, size=t, replace=F)
		a = matrix(0, nrow=q, ncol=t, byrow=T)
			for (i in seq(1, t)){a[,i]=sample(c(1, rep(0, each=q-1)), size=q, replace=F)}
		A = matrix(0, nrow=l, ncol=m)
	A[qtl.loc, tra.loc] = a

	#######
	##	 ##
	## B ##
	##	 ##
	#######
	#transcript (and transcript genotype?!) interactions effects (m x m)
		#transcript additive effects sampling parameters:
			# #beta distribution:
			# 	ta1=2
			# 	ta2=2
			#normal distribution:
				ta1=0 #mean additive transcript effect
				ta2=4 #additive transcript variance
		#transcript interaction effects sampling parameters:
			# #beta distribution:
			# 	ti1=0.25
			# 	ti2=1
			#normal distribution:
				ti1=0 #mean additive transcript effect
				ti2=1 #additive transcript variance
		# tra.additive = matrix(rbeta(n=t, shape1=ta1, shape2=ta2), nrow=t, ncol=1)
		# tra.additive = tra.additive/sum(tra.additive)
		tra.additive = matrix(rnorm(n=t, mean=ta1, sd=sqrt(ta2)), nrow=t, ncol=1)
		b_vect = matrix(0, nrow=m, ncol=1)
		b_vect[tra.loc,1] = tra.additive
	B = diag(b_vect[,1]) #----> no interactions - completely additive transcript effects! - off-diagonals are all zeros!
		if (additiveTranscriptEffectsOnly == FALSE) {
				# tra.interactions = matrix(rbeta(n=q*q, shape1=ti1, shape2=ti2), nrow=q, ncol=q, byrow=TRUE)
				# tra.interactions = tra.interactions/sum(tra.interactions)
				tra.interactions = matrix(rnorm(n=q*q, mean=ti1, sd=sqrt(ti2)), nrow=q, ncol=q, byrow=TRUE)
				tra.BxI = tra.interactions
				diag(tra.BxI) = tra.additive
				b_matr = matrix(0, nrow=m, ncol=m)
				b_matr[tra.loc, tra.loc] = tra.BxI
			B = b_matr # ---> additive (diagonals) and interaction (off-diagonals) effects
				#Assesing tra effects:
					#tra additive effects distribution
						#plot(density(tra.additive))
					#tra interaction effects distribution
						#plot(density(tra.interactions))
		}
	# #assesing transcript effects distributions:
	# plot(density(B[B!=0])) #whole matrix
	# plot(density(diag(B)[diag(B)!=0])) #addtive effects only
	# d = row(B) - col(B)
	# plot(density(B[d!=0][B[d!=0]!=0])) #all non-additive effects

	#######
	##	 ##
	## J ##
	##	 ##
	#######
	#vector of ones for summing up all effects per individual
	j = matrix(1, nrow=m, ncol=1)

	#######
	##	 ##
	## e ##
	##	 ##
	#######
	#residual effects ~ N(0, sd=??? --> such that h2=50%)
	if (model == 0) {
		Vmodel = var(X %*% K %*% j)
		Verror = k*Vmodel
		e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
	} else if (model == 1) {
		Vmodel = var(X %*% K %*% A %*% B %*% j)
		Verror = k*Vmodel
		e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
	} else if (model == 2) {
		Vmodel = var((X %*% K %*% j) + (X %*% K %*% A %*% B %*% j))
		Verror = k*Vmodel
		e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
	}

	#######
	##	 ##
	## Y ##
	##	 ##
	#######
	#phenotypes
	if (model == 0) {
		#snp model
		Y = (X %*% K %*% j) + e
	} else if (model == 1) {
		#fully multiplicative
		Y = (X %*% K %*% A %*% B %*% j) + e
	} else if (model ==2){
		#additive-multiplicative?
		Y = (X %*% K %*% j) + (X %*% K %*% A %*% B %*% j) + e
	}
	# jpeg("Density Plot of Simulated Phenotypes.jpeg")
	# plot(density(Y))
	# dev.off()

	#######
	##	 ##
	## T ##
	##	 ##
	#######
	#transcript abundance data - all transcripts
	t_causal = X %*% K %*% A #scaled abundance matrix of causal transcripts
	# causal trasnctipts' abundance data is supposedly normally distributed:
	# plot(density(t_causal[,tra.loc]))
	# qqnorm(t_causal[,tra.loc]); qqline(t_causal[,tra.loc])
	# shapiro.test(t_causal[,tra.loc])
	T = matrix(rnorm(n=n*m, mean=mean(t_causal[,tra.loc]), sd=sd(t_causal[,tra.loc])), nrow=n, ncol=m, byrow=T)
	T[,tra.loc] = t_causal[,tra.loc]
	T = T + (-min(T)) # such that min(T) == 0
	T = T^2 # folding to simulate a chi-square distribution as seen empirically - more realistic?!

	##############
	###		   ###
	### OUTPUT ###
	###		   ###
	##############
	OUT = list("X"=X, "T"=T, "Y"=Y, "qtl.loc"=qtl.loc, "qtl.additive"=qtl.additive, "tra.loc"=tra.loc, "tra.additive"=tra.additive)
	return(OUT)
}