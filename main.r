#!/usr/bin/env Rscript
# production code to simulate: (1) individual geno & pheno (GWAS), (2) pooled geno & individual pheno (GWAplha), and (3) pooled geno & pheno (GWAlpha)
# parallelize me via: "parallelize_main.sh"
#model1: Y1 = XKABj + e; model2: Y2 = XC1 + XKABj + e;
args = commandArgs(trailingOnly=TRUE)
model = as.integer(args[1])			#0=gene effects alone; 1=fully multiplicative; 2=additive-multiplicative
n = as.integer(args[2])				#number of individuals
l = as.integer(args[3]) 			#number of loci
m = as.integer(args[4]) 			#number of transcripts (== number of loci)
q = as.integer(args[5])				#number of causal loci or QTL (can sampled from some distribution)
t = as.integer(args[6]) 			#number of causal transcripts (can sampled from some distribution)
k = as.numeric(args[7])				#Verror / Vmodel
trait = as.integer(args[8])			#trait state (for GWAlpha_2 and TWAlpha_2 only): 0=quantitative mean; 1=binary survival rate (use 1 for my herbicide resistance per tray pooled phenotyping methodology)
fileNamesSuffix = args[9]			#output filenames suffix
iterations = as.integer(args[10])	# number of simulation iterations
# #for testing:
	# model=0
	# n = 500
	# l = 1000
	# m = l
	# q = 10
	# t = q
	# k = 1
	# trait = 0
	# fileNamesSuffix = "Full_Model"
	# iterations = 2

#############################
######## DEFINE FUNCTIONS
#############################
source("simulate_genotypes_transcripts_phenotypes.r")
source("GWAS_TWAS.r")
source("GWAlpha.r")
source("TWAlpha.r")
source("calculate_ROC_AUC.r")
#END OF FUNCTION DEFINITIONS#

#############################
####### EXECUTION SCRIPT
#############################
AUC_GWAS = c()
PER_GWAS = c()
AUC_TWAS = c()
AUC_GWAlpha_1 = c()
AUC_TWAlpha_1 = c()
AUC_GWAlpha_2 = c()
AUC_TWAlpha_2 = c()

for (iter in 1:iterations)
{
	#################################
	###							  ###
	### S I M U L A T E   D A T A ###
	###							  ### simulate genomes, transcriptomes and phenotypes
	################################# LD simulated
	SIMULATED = simulate_genomes_transcriptomes_phenotypes(model, n, l, m, q, t, k)
	X = SIMULATED$X
	T = SIMULATED$T
	Y = SIMULATED$Y
	qtl.loc = SIMULATED$qtl.loc
	qtl.additive = SIMULATED$qtl.additive
	tra.loc = SIMULATED$tra.loc
	tra.additive = SIMULATED$tra.additive
	
	qtl.labels = matrix(0, nrow=l, ncol=1)
	qtl.labels[qtl.loc,1] = 1
	tra.labels = matrix(0, nrow=m, ncol=1)
	tra.labels[tra.loc,1] = 1

	# plot(density(Y))	# ~normally distributed
	# plot(density(X))	# 3 peaks: 0, 1, 2
	# plot(density(T))	# ~chi-square distributed - folded normal dist'n

	#################################
	###							  ###
	###         G  W  A  S        ###
	###							  ### individual genoptypes
	################################# individual phenotypes
	LOD_GWAS = GWAS_TWAS(Y, X)
	ROC_GWAS = simple_roc.bobHorton(qtl.labels, LOD_GWAS)
	AUC_GWAS = c(AUC_GWAS, simple_auc.jef(ROC_GWAS$TPR, ROC_GWAS$FPR))
	bonferroni.cutoff = -log(0.05/n)
	qtl = (qtl.labels * LOD_GWAS)
	found = qtl[qtl>=bonferroni.cutoff]
	PER_GWAS = c(PER_GWAS, length(found)/q)

	par(mfrow=c(1,2))
	plot(1:length(LOD_GWAS), LOD_GWAS, type="p", pch=20, col="gray", ylab=expression("-log"[10]*"(p-value)"), xlab="SNP ID")
	points(qtl.loc, LOD_GWAS[qtl.loc], type="p", pch=10, col="red")
	lines(x=0:l, y=rep(bonferroni.cutoff, each=l+1), lty=3)
	plot(ROC_GWAS$FPR, ROC_GWAS$TPR, type="l", col="red", xlab="False Positive Rate", ylab="True Positive Rate")
	legend("bottomright", legend=paste("AUC=", round(AUC_GWAS[iter], 2), sep=""))
	legend("bottomleft", legend=paste("DETECTED=", round(PER_GWAS[iter]*100, 0), "%", sep=""))

	#################################
	###							  ###
	###         T  W  A  S        ###
	###							  ### individual RNAseq
	################################# individual phenotypes
	LOD_TWAS = GWAS_TWAS(Y, T)
	ROC_TWAS = simple_roc.bobHorton(tra.labels, LOD_TWAS)
	AUC_TWAS = c(AUC_TWAS, simple_auc.jef(ROC_TWAS$TPR, ROC_TWAS$FPR))

	#################################
	###							  ###
	###		G W A l p h a _ 1	  ###
	###							  ### pooled genotypes
	################################# individual phenotypes
	# (1) PHENOTYPING
	#arrange by phenotypic values
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	ALL = ALL[order(ALL$Y), ]
	PHEN = matrix(ALL[, 2])
	GENO = ALL[, (2+1):(2+m)]
	TRAN = ALL[, (2+m+1):(2+m+m)]
	#pooling: make sure mod(n, npool) == 0
	npool = round(n/100) #set the number of individuals per pool to 100 and make sure the number of pools is a natural number > 1
	for (i in 1:npool) {
		index1 = ((n/npool)*(i-1)) + 1
		index2 = (n/npool)*i
		assign(sprintf("PHEN_pool.%02d", i), PHEN[index1:index2,])
		assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
		assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])
	}
	perc = seq(0, 1, by=1/npool); perc = perc[2:(length(perc)-1)]

	# (2) GENOTYPING
	# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
	SYNC = c()
	for (p in ls()[grepl("GENO_pool.", ls())]) {
		p = eval(parse(text=p))
		sync.A = colSums(p)
		sync.T = (2*nrow(p)) - sync.A
		sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
		sync = apply(sync, 1, paste, collapse=":")
		SYNC = cbind(SYNC, sync)
	}
	SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)

	# (3) GWAlpha
	SD = sqrt(var(PHEN[,1]))
	MIN = min(PHEN[,1])
	MAX = max(PHEN[,1])
	PERC = perc
	ALPHA_GWAlpha_1 = GWAlpha(SYNC, SD, MIN, MAX, PERC)
	ROC_GWAlpha_1 = simple_roc.bobHorton(qtl.labels, ALPHA_GWAlpha_1)
	AUC_GWAlpha_1 = c(AUC_GWAlpha_1, simple_auc.jef(ROC_GWAlpha_1$TPR, ROC_GWAlpha_1$FPR))

	plot(1:length(ALPHA_GWAlpha_1), ALPHA_GWAlpha_1, type="p", pch=20, col="gray", ylab=expression("-log"[10]*"(p-value)"), xlab="SNP ID")
	points(qtl.loc, ALPHA_GWAlpha_1[qtl.loc], type="p", pch=10, col="red")
	
	plot(ROC_GWAlpha_1$FPR, ROC_GWAlpha_1$TPR, type="l", col="red", xlab="False Positive Rate", ylab="True Positive Rate")
	legend("bottomright", legend=paste("AUC=", round(AUC_GWAlpha_1[iter], 2), sep=""))

	#clean-up:
	rm(list=ls()[grepl("PHEN_pool.", ls())])
	rm(list=ls()[grepl("GENO_pool.", ls())])
	rm(list=ls()[grepl("TRAN_pool.", ls())])

	#################################
	###							  ###
	###		T W A l p h a _ 1	  ###
	###							  ### pooled RNAseq
	################################# individual phenotypes
	# (1) PHENOTYPING
	#arrange by phenotypic values
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	ALL = ALL[order(ALL$Y), ]
	PHEN = matrix(ALL[, 2])
	GENO = ALL[, (2+1):(2+m)]
	TRAN = ALL[, (2+m+1):(2+m+m)]
	#pooling: make sure mod(n, npool) == 0
	npool = round(n/100) #set the number of individuals per pool to 100 and make sure the number of pools is a natural number > 1
	for (i in 1:npool) { 
		index1 = ((n/npool)*(i-1)) + 1
		index2 = (n/npool)*i
		assign(sprintf("PHEN_pool.%02d", i), PHEN[index1:index2,])	#(n individuals x 1 pehnotype)
		assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])	#(n individuals x l loci)
		assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])	#(n individuals x m transcripts)
	}
	perc = seq(0, 1, by=1/npool); perc = perc[2:(length(perc)-1)]

	# (2) RNAseq
	# pooled RNAseq data (transform pooled RNAseq data ~(0,1)per transcript across pools)
	RNA = c()
	for (p in ls()[grepl("TRAN_pool.", ls())]) {
		p = eval(parse(text=p))
		RNA = cbind(RNA, colSums(p)) #(m transcripts x p pools)
	}
	# divide transcript abundance by the number of individuals per pool
	TRANC = c()
	for (i in 1:npool) {
		p = eval(parse(text=ls()[grepl("TRAN_pool.", ls())][i]))
		TRANC = cbind(TRANC, RNA[,i]/nrow(p))
	}
	TRANC = (TRANC-apply(TRANC, MARGIN=1, min)) + 1 # added 1 just so that we don't have 0's which results in optimization failure in TWAlpha.r +++ this improves the power!!!!20180112
	TRANC = TRANC/rowSums(TRANC)
	
	# (3) TWAlpha
	SD = sqrt(var(PHEN[,1]))
	MIN = min(PHEN[,1])
	MAX = max(PHEN[,1])
	PERC = perc
	ALPHA_TWAlpha_1 = TWAlpha(TRANC, SD, MIN, MAX, PERC)
	ROC_TWAlpha_1 = simple_roc.bobHorton(tra.labels, ALPHA_TWAlpha_1)
	AUC_TWAlpha_1 = c(AUC_TWAlpha_1, simple_auc.jef(ROC_TWAlpha_1$TPR, ROC_TWAlpha_1$FPR))

	#clean-up:
	rm(list=ls()[grepl("PHEN_pool.", ls())])
	rm(list=ls()[grepl("GENO_pool.", ls())])
	rm(list=ls()[grepl("TRAN_pool.", ls())])

	#################################
	###							  ###
	###		G W A l p h a _ 2	  ###
	###							  ### pooled genotypes
	################################# pooled phenotypes
	# (1) PHENOTYPING
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	#pooling: make sure mod(n, npool) == 0
	#STEPS:
			npool = round(n/100)    #set the number of individuals per pool to 100 and make sure the number of pools is a natural number > 1
            #(a) identify 2 pools of 50 individuals each that are highly susceptible and highly resistant then randomize everything in between
				ind.pool = n/npool
				SUS = ALL[order(ALL$Y),][1:ind.pool, ]
				RES = ALL[order(ALL$Y),][(nrow(ALL)-(ind.pool-1)):nrow(ALL), ]
			#(b) randomize in between (this introduces stochasticity in the AUC!!!!)
				BET = ALL[order(ALL$Y),][(ind.pool+1):(nrow(ALL)-(ind.pool)), ]; BET = BET[sample(1:nrow(BET), size=nrow(BET), replace=F), ]
			#(c) generate the pools prior to systematic contamination with SUS or RES pools
				ALL2 = rbind(SUS, BET, RES)
				PHEN = matrix(ALL2[, 2])
				GENO = ALL2[, (2+1):(2+l)]
				TRAN = ALL2[, (2+l+1):(2+l+m)]
				for (i in 1:npool) { 
					index1 = ((n/npool)*(i-1)) + 1
					index2 = (n/npool)*i
					assign(sprintf("PHEN_pool.%02d", i), PHEN[index1:index2,])
					assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
					assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])
				}
			#(d) contamination with SUS or RES (KEY STEP IN BULK PHENOTYPING!!!)
				PHEN_SUS = matrix(SUS[, 2])
				GENO_SUS = SUS[, (2+1):(2+m)]
				TRAN_SUS = SUS[, (2+m+1):(2+m+m)]
				PHEN_RES = matrix(RES[, 2])
				GENO_RES = RES[, (2+1):(2+m)]
				TRAN_RES = RES[, (2+m+1):(2+m+m)]
				#SUS contamination
					#if npool is even:
					if (npool%%2==0) {
						for (i in 1:((npool-2)/2)) {
							j = i + 1
							k = 1 - (i/((npool-1)/2))
							assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_SUS[sample(1:length(PHEN_SUS), size=round(k*length(PHEN_SUS)), replace=F)]))
							assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_SUS[sample(1:nrow(GENO_SUS), size=round(k*nrow(GENO_SUS)), replace=F), ]))
							assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_SUS[sample(1:nrow(TRAN_SUS), size=round(k*nrow(TRAN_SUS)), replace=F), ]))
						}
					} else {
						#if npool is odd:
						for (i in 1:((npool-1)/2)) {
							#include the center pool (npool-1 instead of npool-2)
							j = i + 1
							k = 1 - (i/((npool-1)/2))
							assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_SUS[sample(1:length(PHEN_SUS), size=round(k*length(PHEN_SUS)), replace=F)]))
							assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_SUS[sample(1:nrow(GENO_SUS), size=round(k*nrow(GENO_SUS)), replace=F), ]))
							assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_SUS[sample(1:nrow(TRAN_SUS), size=round(k*nrow(TRAN_SUS)), replace=F), ]))
						}
					}
				#RES contamination
					#if npool is even:
					if (npool%%2==0) {
						for (i in 1:((npool-2)/2)) {
							j = (npool/2) + i
							k = i/((npool-1)/2)
							assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_RES[sample(1:length(PHEN_RES), size=round(k*length(PHEN_RES)), replace=F)]))
							assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_RES[sample(1:nrow(GENO_RES), size=round(k*nrow(GENO_RES)), replace=F), ]))
							assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_RES[sample(1:nrow(TRAN_RES), size=round(k*nrow(TRAN_RES)), replace=F), ]))
						}
					} else {
						#if npool is odd:
						for (i in 1:((npool-3)/2)) {
							#exclude the center pool (npool-3 instead of npool-2)
							j = ceiling(npool/2) + i
							k = i/((npool-1)/2)
							assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_RES[sample(1:length(PHEN_RES), size=round(k*length(PHEN_RES)), replace=F)]))
							assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_RES[sample(1:nrow(GENO_RES), size=round(k*nrow(GENO_RES)), replace=F), ]))
							assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_RES[sample(1:nrow(TRAN_RES), size=round(k*nrow(TRAN_RES)), replace=F), ]))
						}
					}
			#(e) build distrbution of phenotypic values across pools
				poolMEANS = c()
				poolCUMM = c(0)
				if(trait==0) {
					#trait is quantitative and extracting the mean is effective & efficient
					for (i in ls()[grepl("PHEN_pool.", ls())]) {
						dat = eval(parse(text=i))
						poolMEANS = c(poolMEANS, mean(dat))
						poolCUMM = c(poolCUMM, poolCUMM[length(poolCUMM)]+length(dat))
						#print(i); print(length(dat)); print(mean(dat))
					}
				} else if(trait==1) {
					#trait is binary: e.g. individual plants per pool are either dead or alive - so the mean trait value per pool will be the survival rate
					threshold_for_survival = mean(RES$Y)/2 # set threshold at half of the mean of the resistant pool - that is values lower than this indicates susceptibility
					for (i in ls()[grepl("PHEN_pool.", ls())]) {
						dat = eval(parse(text=i))
						poolMEANS = c(poolMEANS, sum(dat>threshold_for_survival)/length(dat))
						poolCUMM = c(poolCUMM, poolCUMM[length(poolCUMM)]+length(dat))
						#print(i); print(length(dat)); print(mean(dat))
					}
				} else {
					print("Invalid trait state value. Set 0 for quantitative mean and 1 for binary survival rate")
				}
				#plot(density(poolMEANS))
				perc = poolCUMM/max(poolCUMM); perc = perc[2:(length(perc)-1)]

	# (2) GENOTYPING 
	# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
	SYNC = c()
	for (p in ls()[grepl("GENO_pool.", ls())]) {
		p = eval(parse(text=p))
		sync.A = colSums(p)
		sync.T = (2*nrow(p)) - sync.A
		sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
		sync = apply(sync, 1, paste, collapse=":")
		SYNC = cbind(SYNC, sync)
	}
	SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)

	# (3) GWAlpha
	SD = sqrt(var(poolMEANS))
	MIN = min(poolMEANS)
	MAX = max(poolMEANS)
	PERC = perc
	ALPHA_GWAlpha_2 = GWAlpha(SYNC, SD, MIN, MAX, PERC)
	ROC_GWAlpha_2 = simple_roc.bobHorton(qtl.labels, ALPHA_GWAlpha_2)
	AUC_GWAlpha_2 = c(AUC_GWAlpha_2, simple_auc.jef(ROC_GWAlpha_2$TPR, ROC_GWAlpha_2$FPR))

	#clean-up:
	rm(list=ls()[grepl("PHEN_pool.", ls())])
	rm(list=ls()[grepl("GENO_pool.", ls())])
	rm(list=ls()[grepl("TRAN_pool.", ls())])

	#################################
	###							  ###
	###		T W A l p h a _ 2	  ###
	###							  ### pooled RNAseq
	################################# pooled phenotypes
	# (1) PHENOTYPING
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	#pooling: make sure mod(n, npool) == 0
	#STEPS:
			npool = round(n/100)    #set the number of individuals per pool to 100 and make sure the number of pools is a natural number > 1
            #(a) identify 2 pools of 50 individuals each that are highly susceptible and highly resistant then randomize everything in between
				ind.pool = n/npool
				SUS = ALL[order(ALL$Y),][1:ind.pool, ]
				RES = ALL[order(ALL$Y),][(nrow(ALL)-(ind.pool-1)):nrow(ALL), ]
			#(b) randomize in between (this introduces stochasticity in the AUC!!!!)
				BET = ALL[order(ALL$Y),][(ind.pool+1):(nrow(ALL)-(ind.pool)), ]; BET = BET[sample(1:nrow(BET), size=nrow(BET), replace=F), ]
			#(c) generate the pools prior to systematic contamination with SUS or RES pools
				ALL2 = rbind(SUS, BET, RES)
				PHEN = matrix(ALL2[, 2])
				GENO = ALL2[, (2+1):(2+l)]
				TRAN = ALL2[, (2+l+1):(2+l+m)]
				for (i in 1:npool) { 
					index1 = ((n/npool)*(i-1)) + 1
					index2 = (n/npool)*i
					assign(sprintf("PHEN_pool.%02d", i), PHEN[index1:index2,])
					assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
					assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])
				}
			#(d) contamination with SUS or RES (KEY STEP IN BULK PHENOTYPING!!!)
				PHEN_SUS = matrix(SUS[, 2])
				GENO_SUS = SUS[, (2+1):(2+m)]
				TRAN_SUS = SUS[, (2+m+1):(2+m+m)]
				PHEN_RES = matrix(RES[, 2])
				GENO_RES = RES[, (2+1):(2+m)]
				TRAN_RES = RES[, (2+m+1):(2+m+m)]
				#SUS contamination
					#if npool is even:
					if (npool%%2==0) {
						for (i in 1:((npool-2)/2)) {
							j = i + 1
							k = 1 - (i/((npool-1)/2))
							assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_SUS[sample(1:length(PHEN_SUS), size=round(k*length(PHEN_SUS)), replace=F)]))
							assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_SUS[sample(1:nrow(GENO_SUS), size=round(k*nrow(GENO_SUS)), replace=F), ]))
							assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_SUS[sample(1:nrow(TRAN_SUS), size=round(k*nrow(TRAN_SUS)), replace=F), ]))
						}
					} else {
						#if npool is odd:
						for (i in 1:((npool-1)/2)) {
							#include the center pool (npool-1 instead of npool-2)
							j = i + 1
							k = 1 - (i/((npool-1)/2))
							assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_SUS[sample(1:length(PHEN_SUS), size=round(k*length(PHEN_SUS)), replace=F)]))
							assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_SUS[sample(1:nrow(GENO_SUS), size=round(k*nrow(GENO_SUS)), replace=F), ]))
							assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_SUS[sample(1:nrow(TRAN_SUS), size=round(k*nrow(TRAN_SUS)), replace=F), ]))
						}
					}
				#RES contamination
					#if npool is even:
					if (npool%%2==0) {
						for (i in 1:((npool-2)/2)) {
							j = (npool/2) + i
							k = i/((npool-1)/2)
							assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_RES[sample(1:length(PHEN_RES), size=round(k*length(PHEN_RES)), replace=F)]))
							assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_RES[sample(1:nrow(GENO_RES), size=round(k*nrow(GENO_RES)), replace=F), ]))
							assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_RES[sample(1:nrow(TRAN_RES), size=round(k*nrow(TRAN_RES)), replace=F), ]))
						}
					} else {
						#if npool is odd:
						for (i in 1:((npool-3)/2)) {
							#exclude the center pool (npool-3 instead of npool-2)
							j = ceiling(npool/2) + i
							k = i/((npool-1)/2)
							assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_RES[sample(1:length(PHEN_RES), size=round(k*length(PHEN_RES)), replace=F)]))
							assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_RES[sample(1:nrow(GENO_RES), size=round(k*nrow(GENO_RES)), replace=F), ]))
							assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_RES[sample(1:nrow(TRAN_RES), size=round(k*nrow(TRAN_RES)), replace=F), ]))
						}
					}
			#(e) build distrbution of phenotypic values across pools
				poolMEANS = c()
				poolCUMM = c(0)
				if(trait==0) {
					#trait is quantitative and extracting the mean is effective & efficient
					for (i in ls()[grepl("PHEN_pool.", ls())]) {
						dat = eval(parse(text=i))
						poolMEANS = c(poolMEANS, mean(dat))
						poolCUMM = c(poolCUMM, poolCUMM[length(poolCUMM)]+length(dat))
						#print(i); print(length(dat)); print(mean(dat))
					}
				} else if(trait==1) {
					#trait is binary: e.g. individual plants per pool are either dead or alive - so the mean trait value per pool will be the survival rate
					threshold_for_survival = mean(RES$Y)/2 # set threshold at half of the mean of the resistant pool - that is values lower than this indicates susceptibility
					for (i in ls()[grepl("PHEN_pool.", ls())]) {
						dat = eval(parse(text=i))
						poolMEANS = c(poolMEANS, sum(dat>threshold_for_survival)/length(dat))
						poolCUMM = c(poolCUMM, poolCUMM[length(poolCUMM)]+length(dat))
						#print(i); print(length(dat)); print(mean(dat))
					}
				} else {
					print("Invalid trait state value. Set 0 for quantitative mean and 1 for binary survival rate")
				}
				#plot(density(poolMEANS))
				perc = poolCUMM/max(poolCUMM); perc = perc[2:(length(perc)-1)]

	# (2) RNAseq
	# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
	SYNC = c()
	for (p in ls()[grepl("GENO_pool.", ls())]) {
		p = eval(parse(text=p))
		sync.A = colSums(p)
		sync.T = (2*nrow(p)) - sync.A
		sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
		sync = apply(sync, 1, paste, collapse=":")
		SYNC = cbind(SYNC, sync)
	}
	SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)

	# (2) RNAseq
	# pooled RNAseq data
	RNA = c()
	for (p in ls()[grepl("TRAN_pool.", ls())]) {
		p = eval(parse(text=p))
		RNA = cbind(RNA, colSums(p)) #(m transcripts x p pools)
	}
	# divide transcript abundance by the number of individuals per pool
	TRANC = c()
	for (i in 1:npool) {
		p = eval(parse(text=ls()[grepl("TRAN_pool.", ls())][i]))
		TRANC = cbind(TRANC, RNA[,i]/nrow(p))
	}
	TRANC = (TRANC-apply(TRANC, MARGIN=1, min)) + 1 # added 1 just so that we don't have 0's which results in optimization failure in TWAlpha.r +++ this improves the power!!!!20180112
	TRANC = TRANC/rowSums(TRANC)
	
	# (3) TWAlpha
	SD = sqrt(var(PHEN[,1]))
	MIN = min(PHEN[,1])
	MAX = max(PHEN[,1])
	PERC = perc
	ALPHA_TWAlpha_2 = TWAlpha(TRANC, SD, MIN, MAX, PERC)
	ROC_TWAlpha_2 = simple_roc.bobHorton(tra.labels, ALPHA_TWAlpha_2)
	AUC_TWAlpha_2 = c(AUC_TWAlpha_2, simple_auc.jef(ROC_TWAlpha_2$TPR, ROC_TWAlpha_2$FPR))

	#clean-up:
	rm(list=ls()[grepl("PHEN_pool.", ls())])
	rm(list=ls()[grepl("GENO_pool.", ls())])
	rm(list=ls()[grepl("TRAN_pool.", ls())])


	print(iter)

}

#########################
########################
######################
####################
#  ULTIMATE OUTPUT
####################
######################
########################
#########################

OUT = data.frame(AUC_GWAS, AUC_TWAS, AUC_GWAlpha_1, AUC_TWAlpha_1, AUC_GWAlpha_2, AUC_TWAlpha_2)
write.csv(OUT, file=paste("OUT_inferFromGENOalone_GWASvsGWAlphas-", fileNamesSuffix, ".csv", sep=""), row.names=F)
