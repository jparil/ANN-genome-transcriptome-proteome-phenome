#Rscript source function: synthethic pooled TWAS or TWAlpha using maximum likelihood estimation

########### IN NEED OF GANGANTUAN EDITING!!! BUT FIRST FIX TRANSCRIPTOME SIMULATION +++>>> MAKE 'EM REALISTIC

# INPUTS:
#1	TRANC -->	matrix of transformed transcript abundance across pools (for each transcript, transformed abundance data across pools sums to 1)
#2	SD -->		standard deviation of phenotype data across all pools
#3	MIN -->		minimum phenotypic values across all pools
#4	MAX -->		maximum phenotypic values across all pools
#5	PERC -->	list of percentile values, e.g. c(0.2, 0.4, 0.6, 0.8) for 5 pools (1 x (npools-1))

# OUTPUT:
#1	ALPHA -->	list of test statistic alpha across all loci or transcripts (1xm)

# compute alpha for each loci:
compute_transcript_Alpha <- function(FREQ_MATRIX, SD, MIN, MAX, PERC) {
	#determine the size of each pool based on their percentile values
	# BINS = c(PERC, 1) - c(0, PERC)
	# #determine the size of each pool based on their percentile values
	# FREQ_A = FREQ_MATRIX
	# FREQ_B = 1-FREQ_A; FREQ_B = FREQ_B/sum(FREQ_B)
	# #transform the allele frequency of the major allele such that the sum of allele frequencies across pools is equal to 1
	# BIN_A = FREQ_A*BINS/sum(FREQ_A*BINS); BIN_A = BIN_A/sum(BIN_A) #is dividing them again with the sum necessary???
	# #transform the aggregate of the alternative alleles in the same way as in the major allele
	# BIN_B = (1 - FREQ_A)*BINS/(1-sum(FREQ_A*BINS));	BIN_B = BIN_B/sum(BIN_B)
	BIN_A = FREQ_MATRIX
	BIN_B = rev(BIN_A) # reverse order of BIN_A to simulate the pseudo-alternative transcript state ---IS THIS FINE???!!!---
	#convert back the transformed allele frequencies into percentiles
	PERC_A = c(); PERC_B = c()
	for(i in 1:length(BIN_A)){
		PERC_A = c(PERC_A, sum(BIN_A[1:i]))
		PERC_B = c(PERC_B, sum(BIN_B[1:i]))
	}
	#define the likelihood function as the cdf(yi, b1,b2) - cdf(yi-1, b1,b2) [--> for the major allele] PLUS cdf(yi, b3,b4) - cdf(yi-1, b3,b4) [--> for the altenative alleles aggregate]
	optimize_me <- function(PERC_A, PERC_B, par) {
		PERC_A0 = c(0, PERC_A[1:(length(PERC_A)-1)])
		PERC_B0 = c(0, PERC_B[1:(length(PERC_B)-1)])
		return(-sum(log(pbeta(PERC_A, shape1=par[1], shape2=par[2])-pbeta(PERC_A0, shape1=par[1], shape2=par[2]))) - sum(log(pbeta(PERC_B, shape1=par[3], shape2=par[4])-pbeta(PERC_B0, shape1=par[3], shape2=par[4]))) )
	}
	#maximize the likelihood or in the case minimize the -log likelihood
	OPTIM_PAR = tryCatch(optim(par=c(1,1,1,1), fn=optimize_me, control=list(reltol=10e-8), PERC_A=PERC_A, PERC_B=PERC_B)$par, error=function(e){return(c(1,1,1,1))})
	#compute for the mean of the beta desitribution based on the computed parameters for both major and alternative alleles
	MU_A = MIN + ((MAX-MIN)*OPTIM_PAR[1]/(OPTIM_PAR[1]+OPTIM_PAR[2]))
	MU_B = MIN + ((MAX-MIN)*OPTIM_PAR[3]/(OPTIM_PAR[3]+OPTIM_PAR[4]))
	#compute the test statistic alpha
	ALPHA = (MU_A - MU_B)/SD #no penalization W = 2*sqrt(pA*(1-pA))
	return(ALPHA)
}


#compute alpha across all loci by calling the previous function: "compute_transcript_Alpha"
TWAlpha <- function(TRANC, SD, MIN, MAX, PERC) {
	alpha = c()
	progressBar = txtProgressBar(min = 0, max = nrow(TRANC), initial = 0, style=3, width=50)
	for (loci in 1:nrow(TRANC)) {
		FREQ_MATRIX = TRANC[loci,]
		out = compute_transcript_Alpha(FREQ_MATRIX, SD, MIN, MAX, PERC)
		alpha = c(alpha, out)
		setTxtProgressBar(progressBar, loci)
	}
	close(progressBar)
	ALPHA = abs(alpha)
	return(ALPHA)
}